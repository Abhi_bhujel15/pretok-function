-- FUNCTION: public.ppwr_tok_pre16(character, bytea, bytea, bytea, character, bigint, character varying, bigint, bigint, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character varying, character, character, character, character varying, character varying, character varying, character varying, character varying, character varying, character, character, character, bytea, character varying, character varying, bytea, bytea, bytea, bytea, bytea, bytea, character varying, character varying, character varying, character varying, bytea, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, bytea, character varying, character varying, bytea, integer, integer, integer, character, integer, bytea, character, bigint, integer, integer, timestamp without time zone, bigint)

-- DROP FUNCTION public.ppwr_tok_pre16(character, bytea, bytea, bytea, character, bigint, character varying, bigint, bigint, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character varying, character, character, character, character varying, character varying, character varying, character varying, character varying, character varying, character, character, character, bytea, character varying, character varying, bytea, bytea, bytea, bytea, bytea, bytea, character varying, character varying, character varying, character varying, bytea, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, bytea, character varying, character varying, bytea, integer, integer, integer, character, integer, bytea, character, bigint, integer, integer, timestamp without time zone, bigint);

CREATE OR REPLACE FUNCTION public.ppwr_tok_pre16(
	pmtid character DEFAULT NULL::bpchar,
	p0 bytea DEFAULT NULL::bytea,
	p1 bytea DEFAULT NULL::bytea,
	p2 bytea DEFAULT '\x00'::bytea,
	p3 character DEFAULT NULL::bpchar,
	p4 bigint DEFAULT NULL::bigint,
	p7 character varying DEFAULT NULL::character varying,
	p8 bigint DEFAULT NULL::bigint,
	p9 bigint DEFAULT NULL::bigint,
	p10 character DEFAULT NULL::bpchar,
	p11 character DEFAULT NULL::bpchar,
	p12 character DEFAULT NULL::bpchar,
	p13 character DEFAULT NULL::bpchar,
	p14 character DEFAULT NULL::bpchar,
	p15 character DEFAULT NULL::bpchar,
	p16 character DEFAULT NULL::bpchar,
	p17 character DEFAULT NULL::bpchar,
	p18 character DEFAULT NULL::bpchar,
	p19 character DEFAULT NULL::bpchar,
	p20 character DEFAULT NULL::bpchar,
	p21 character DEFAULT NULL::bpchar,
	p22 character DEFAULT NULL::bpchar,
	p23 character DEFAULT NULL::bpchar,
	p25 character DEFAULT NULL::bpchar,
	p26 character DEFAULT NULL::bpchar,
	p28 character DEFAULT NULL::bpchar,
	p29 character DEFAULT NULL::bpchar,
	p30 character DEFAULT NULL::bpchar,
	p31 character DEFAULT NULL::bpchar,
	p32 character DEFAULT NULL::bpchar,
	p33 character DEFAULT NULL::bpchar,
	p35 character varying DEFAULT NULL::character varying,
	p37 character DEFAULT NULL::bpchar,
	p38 character DEFAULT NULL::bpchar,
	p39 character DEFAULT NULL::bpchar,
	p41 character varying DEFAULT NULL::character varying,
	p42 character varying DEFAULT NULL::character varying,
	p43 character varying DEFAULT NULL::character varying,
	p44 character varying DEFAULT NULL::character varying,
	p45 character varying DEFAULT NULL::character varying,
	p48 character varying DEFAULT NULL::character varying,
	p49 character DEFAULT NULL::bpchar,
	p50 character DEFAULT NULL::bpchar,
	p51 character DEFAULT NULL::bpchar,
	p52 bytea DEFAULT '\x00'::bytea,
	p53 character varying DEFAULT NULL::character varying,
	p54 character varying DEFAULT NULL::character varying,
	p55_9f36 bytea DEFAULT NULL::bytea,
	p55_95 bytea DEFAULT NULL::bytea,
	p55_9f34 bytea DEFAULT NULL::bytea,
	p55_9f33 bytea DEFAULT NULL::bytea,
	p55_9f27 bytea DEFAULT NULL::bytea,
	p55_9f10 bytea DEFAULT NULL::bytea,
	p60 character varying DEFAULT NULL::character varying,
	p61 character varying DEFAULT NULL::character varying,
	p62 character varying DEFAULT NULL::character varying,
	p63 character varying DEFAULT NULL::character varying,
	p64 bytea DEFAULT NULL::bytea,
	p90 character varying DEFAULT ''::character varying,
	p95 character varying DEFAULT ''::character varying,
	p102 character varying DEFAULT NULL::character varying,
	p103 character varying DEFAULT NULL::character varying,
	p104 character varying DEFAULT NULL::character varying,
	p108 character varying DEFAULT NULL::character varying,
	p112 character varying DEFAULT NULL::character varying,
	p120 character varying DEFAULT NULL::character varying,
	p121 character varying DEFAULT ''::character varying,
	p122 character varying DEFAULT NULL::character varying,
	p123 character varying DEFAULT NULL::character varying,
	p124 character varying DEFAULT NULL::character varying,
	p125 bytea DEFAULT NULL::bytea,
	p126 character varying DEFAULT NULL::character varying,
	p127 character varying DEFAULT NULL::character varying,
	p128 bytea DEFAULT NULL::bytea,
	pac integer DEFAULT 0,
	pcv integer DEFAULT 0,
	pct integer DEFAULT 0,
	pbin character DEFAULT NULL::bpchar,
	ppcnki integer DEFAULT 1,
	ppvv_new bytea DEFAULT '\x'::bytea,
	pcn character DEFAULT 'm'::bpchar,
	pprid bigint DEFAULT 0,
	aav integer DEFAULT 0,
	ppwr_instance_id integer DEFAULT 0,
	pmsg_arrival_time timestamp without time zone DEFAULT NULL::timestamp without time zone,
	ppr_uuid bigint DEFAULT 0)
    RETURNS SETOF type_pre_tok_output_v5 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
   
   ---out put parameter 
	p38o character(6);
	p39o character(2);
	p2to bigint=0;
	p48o character varying;
	ptxn_id character varying;
	pexp integer;
	pinstr character varying;
	p44o character varying;
	pp_info bytea;
	pauth_type integer;
	pclient_id integer;
	prid bigint;
	p7o character varying;
	p12_13o character varying;
	prsn_cde character(3);
	prls_applied character varying='1';
	ppb_tok bigint;
	p112o character varying;
	
	pprod_id integer;
	pvalid_notes character varying;
	pstat_nwk smallint;
	pacc_id bigint;
	pacc_ext_ref character varying;

--------------------------------
   pdo_AVS_resp  BOOLEAN DEFAULT false;   -- to determine if address verification responses needed in 48_83
   p38i  INTEGER DEFAULT 1;
   papprove_so_far  BOOLEAN DEFAULT 1;
   ppvv  BYTEA DEFAULT '\x00';
   ppvv_hsm_generated bytea ;
   pact_exp  CHAR(4) DEFAULT '1299';
   pcpo  VARCHAR(10) DEFAULT '';
   pcad  VARCHAR(20) DEFAULT '';
   patc  SMALLINT DEFAULT 0;
   
   pstat_sys  SMALLINT DEFAULT 0;     -- comment out ... @pvbtok_flags binary(6)   moved to PayCore
   pnDebug  SMALLINT DEFAULT 1;
   psession_id  INTEGER DEFAULT 0;
   p3_1  CHAR(2);
   p3_2  CHAR(2);
   p3_3  CHAR(2);
   p54_S4  DECIMAL(19,4) DEFAULT 0.00;
   pexpnt_loc  SMALLINT DEFAULT 2;
   pexpnt_bill  SMALLINT DEFAULT 2;
   pexpnt_add_amt  SMALLINT DEFAULT 2;
   perr  INTEGER;
   pDE048se38  VARCHAR(1) DEFAULT '0';
   pDE048se71  VARCHAR(40) DEFAULT '0';
   pDE048se61  VARCHAR(6);
   pnote  TEXT;
   p22_1  CHAR(2) DEFAULT '';
   p22_2  CHAR(1) DEFAULT '';
   pD48_S1  CHAR(1) DEFAULT '';
   pi  SMALLINT DEFAULT 2;
   pfid  SMALLINT DEFAULT 0;
   pflen  SMALLINT DEFAULT 0;
   p53_1  CHAR(2) DEFAULT NULL;
   p53_2  CHAR(2) DEFAULT NULL;
   p53_3  CHAR(2) DEFAULT NULL;
   p53_4  SMALLINT DEFAULT NULL;
   p60_1  CHAR(3) DEFAULT '';
   p60_2  CHAR(4) DEFAULT '';
   p60_3  VARCHAR(53) DEFAULT '';
   p120_pc  VARCHAR(20) DEFAULT '';
   p120_addr  VARCHAR(20) DEFAULT '';
   p120_id  CHAR(2);
   p48_82  VARCHAR(2) DEFAULT '';
   pAVS_Supported  BOOLEAN DEFAULT 0;
   p61_1  CHAR(1) DEFAULT '';
   p61_2  CHAR(1) DEFAULT '';
   p61_3  CHAR(1) DEFAULT '';
   p61_4  CHAR(1) DEFAULT '';
   p61_5  CHAR(1) DEFAULT '';
   p61_6  CHAR(1) DEFAULT '';
   p61_7  CHAR(1) DEFAULT '';
   p61_8  CHAR(1) DEFAULT '';
   p61_9  CHAR(1) DEFAULT '';
   p61_10  CHAR(1) DEFAULT '';
   p61_11  CHAR(1) DEFAULT '';
   p61_12  CHAR(2) DEFAULT '';
   p61_13  CHAR(3) DEFAULT '';
   p61_14  CHAR(10) DEFAULT '';
  -- psend_unblock  BOOLEAN DEFAULT 1;
   p63_1  CHAR(3) DEFAULT '';
   p63_2  CHAR(9) DEFAULT '';
   paid  BIGINT;
   ptempvalue  VARCHAR(6);
   pccy_exponent  SMALLINT;
   pti  VARCHAR(20) DEFAULT '';
   ptok_id  INTEGER;
   pbase_ccy  SMALLINT;
   pstatus_sys  SMALLINT=0;
   pstatus_nwk  SMALLINT=0;
   ptok_flags bit(32) DEFAULT '00000000000000000000000000000000';
   pat_in  SMALLINT DEFAULT get_byte(p55_9f36,1);  --CAST(p55_9f36 AS INTEGER);
  
   update_tok character varying DEFAULT'';
   pmti_orig character varying;
   p11_orig character varying;
   p7_orig character varying;
   p32_orig character varying;
   ptxn_yr_loc character varying;
   pserv_code CHAR(3) default '221'; 
   prowcount bigint :=0;
   pdummy_pb_tok bigint;
   pdummy_pr_tok bigint;
   
   	ppin_rem_onl SMALLINT;
	ppin_rem_chp SMALLINT;
	
    txn_checks_applied bigint default 0;
	txn_checks_results bigint default 0;
    	rule_level integer;
	-- by ANil
	cvr_first_byte smallint DEFAULT 0;
	cvr_fourth_byte smallint DEFAULT 0;
	cvr_fifth_byte smallint DEFAULT 0;

	pupdate_tok varchar(500) = '';
	p7_origR char(14)='0101000001';
	pti1 varchar(20);
	pchip_pin_tries smallint=3;
	pcountry char(3) ;
   
  pcountry_vel_count int;
  pkey_id smallint;
	  
pd48_26 character varying;	  
pd48_71 character varying;
pd48_71_sf1 character (2);
pd48_71_sf2 character (2);
pd48_71i  SMALLINT DEFAULT 1;
pd48_71ifid  SMALLINT DEFAULT 0;
de48_71_description character varying;

temp_expiry character varying;
p3ds_status smallint default 0;
tempNote character varying default '';
-- SOH added 29 Mar 2020
ppan_seq character(2) ='01';
p38o_prosa char(6);

--Added by Abhishek fro Fast 2.0 enhancements
o_merchantType character varying default '';
o_transactionCurrency character varying default '';
o_billingCurrency character varying default '';
p37_orig character varying;

--for pre-authorised
_de48_63 character varying(15);
pis_pre_authorise boolean;
pis_recurring boolean;

--multi card
penable_multi_acc_card char(1);
pmulti_account bigint;

script_data character varying;
script_status smallint;
currentStatus smallint;
p_unupdated_counter smallint;

--updating on p_tok column
	pact_final smallint;
	ppin_rem_onl_final smallint;
	ppin_rem_chip_final smallint;
	pstatus_nwk_final smallint;
	p38_final integer;
    ptok_flags_final bit(32);
	ppvv_final bytea ;
	ppvv_hsm_final bytea;
---replacing de48temp table 
    pde48_array text[];
---removing cvv2 from de48
    p48_san character varying;
	se_val character varying;
	
----for psd2
	pde48_22_1 char(2);
	ppsd2_check smallint=0;

---for offline pin check 
	poffline_pin_check smallint=0;
	
---expiry 
	pexpiry_with_timestamp timestamp;
	
	
--hsm status
_hsm_status char(1) ;

--product billing ccy
_prod_bill_ccy char(3);
BEGIN
raise notice '%',p35;
--p48:='T';
raise notice 'p63%',p63;
-- SOH added to add hex values to txn_tok table... into validation_notes field 

-- This function was converted on Tue Oct 03 20:38:38 2017 using Ispirer SQLWays 8.0 Build 5069 64bit Licensed to Paymentology - Shane O'Hara - UK (Standard Project License - Ispirer MnMTK 2017 - MSSQLServer to PostgreSQL SQL Migration, 57.000 LOC, 3 Months, 20171230).

--------AUth Type:  pauth_type -------------------- for external auth ------------------
--  0 = send to FAST (ext auth)
--  1 = Process internally
--  2 = send to handle_auth and PayAuth AND use reply from handle_auth
--  3 = send to handle_auth and PayAuth AND use reply from PayAuth
--  4 = send to handle_auth and PayAuth AND use SAFER reply
-----------------------------------------------------------------------------
--------------------------------------------- declare variables ------------------

   p39o := '00';
   pexp := 1;
   pclient_id :=  10040001;
   pinstr := '00';  
   p112o :=p112;
   pvalid_notes := '';

--------------- @pp_info  is to be used to pass results on to PayCore   -------------------------
   pp_info := 0;

	raise notice 'p39o %',p39o;
-- @pp_info  00 00 00 00 00  
-- @pp_info  00 00 00 00 00  
--  Byte 5 (right) 
-- 00000001 ==> ATC out of Range
-- 00000010 ==> ATC replay
-- 00000100 ==> Not ARQC Cryptogram
-- 00001000 ==> ARQC fail
-- 00010000 ==> no track data
-- 00100000 ==> both tracks
-- 01000000 ==> pan mismatch
-- 10000000 ==> Expiry Mismatch
-- byte 4
-- 00000001 ==> invalid trk separator
-- 00000010 ==> invalid status enquiry chk De4<>0
-- 00000100 ==> invalid status enquiry DE3_1 <>00
-- 00001000 ==> status enquiry all ok.
-- 00010000 ==> AVS Response provided in De48
-- 00100000 ==> CVV1 fail
-- 01000000 ==> CVV2 fail
-- 10000000 ==> CVV2 passed

--------- TOKEN SWAP ------------------------

--encrypt card on .net side.... call token table using encrypted no to get tok
--encrypt expity and service code on .net side and call tok table 2? to get match.

----------------------------------tok_flags --------Flags held at token level for various usees --------------------

--Name             value         Description                                         (bit position)
-----------------------------------------------------------------------------------------------------
--pin_ub      1 Send Pin unblock to card                    1
--pin_new     1 generate new rand pin and send to card on nxt EMV txn     2
--send_bal      1 Send card bal in De54 in next auth                3

--Blk_ecom      1 Sets card to decline all Ecommerce                9
--Blk_atm     1 Sets card to decline all ATM                  10
--Blk_bal_enq   1 Sets card to deline Bal enquiry over auth           11
--Blk_csh_bak   1 Sets card to decline cash back txns               12
--Blk_csh_disb    1 Sets card to decline De3_1 = 17                 13
--Blk_cr_auth   1 Sets card to decline credit pur/refund (DE3_1=20)       14
--Blk_cr_auth   1 Sets card to decline credit deposit (DE3_1=21)          15
--Blk_cr_auth   1 Sets card to decline credit adjust (DE3_1=22)         16
--Blk_cr_auth   1 Sets card to decline chq deposit guarantee (DE3_1=23)     17
--Blk_cr_auth   1 Sets card to decline chq deposit (DE3_1=24)           18
--Blk_cr_auth   1 Sets card to decline credit payment (DE3_1=28)          19
--Blk_cr_auth   1 Sets card to decline non base currency txn            20
--Blk_acc_enq   1 Sets card to decline all account enq (de61_7=8)         21
--Blk_crd_np    1 sets card to decline Card not present txns            22
--Blk_ch_np     1 sets card to decline Cardholder not present txns        23
--Blk_fo_cm_atm   1 sets card to decline failover to mag at emv capable atm     24
--Blk_mag_atm   1 sets card to decline mag at atm                 25
--Alow_fallbk_cac 1 Allow if fallback chp to mag at chp capable pos DE22_1=80   26  
--Alow_mag_atm    1 Allow if de22_1='02' & De48_88 ='Y'               27  (compliance downgrade - acq not qualified to submit mag)

--Ext_Auth_OFF    1 Set card to External Auth OFF                 32

-----------------------------------------------------------------------------------
 
--- prepare p7o and p12_13o  (adding Year to local till datetime and processing datetime)

  raise notice 'incmoing de7 % de12 % de13 %',p7,p12,p13;
  if date_part('month', now())  = CAST(left(p7,2) AS SMALLINT)  then
      ptxn_yr_loc = date_part('year', now());
  elseif date_part('month', now())  = 1 and left(p7,2) = '12' then
        ptxn_yr_loc = date_part('year', now())-1;
    elseif date_part('month', now())  = 12 and left(p7,2) = '01' then
        ptxn_yr_loc = date_part('year', now())+1;
    	end if;
	 	ptxn_yr_loc =coalesce(ptxn_yr_loc,date_part('year', now())::character varying) ;

-- SOH 04 Apr 2020 ... improved way to detect year .. NOTE DE7 is UTC... this makes year easy.
		p7o := DATE_PART('year', now() at time zone 'utc') || p7;
	  
	  if (p12 is null) then   
	  p12=substring(p7,5,6);   --hhmmss
	  end if;
	  if (p13 is null) then
	  p13 =substring(p7,1,4);  --MMDD
  	  End if;
	  
		   	if date_part('month', now())  = CAST(left(p13,2) AS SMALLINT)  then
			p12_13o := date_part('year', now()) || p13 ||  P12;
			elseif date_part('month', now())  = 1 and left(p13,2) = '12' then
			p12_13o := date_part('year', now())-1 || p13 ||  P12;
			elseif date_part('month', now())  = 12 and left(p13,2) = '01' then
			p12_13o := date_part('year', now())+1 || p13 ||  P12;
			elseif date_part('month', now())  > CAST(left(p13,2) AS SMALLINT)  then
			p12_13o := date_part('year', now()) || p13 ||  P12;
			elseif date_part('month', now())  < CAST(left(p13,2) AS SMALLINT)  then
			p12_13o := date_part('year', now()) || p13 ||  P12;
	    	end if;
	
	  
  raise notice '70 %, 12_13 %',p7o,p12_13o;
   
    
-- 	If the transaction date is not known at the time of authorization (for example, in the case of a
--     preauthorization request), the acquirer must use the authorization date in this field.
	  if p12_13o is null or p13='0000' then 
	    p12_13o =p7o;
	 end if;	
   --if system time is 03:00 01 jan 2018 and   till date is 1231 230000

  
if pMTID = '0190' then

select p2to = De2,  ptxn_id = txn_id from txn_tok  where dell = p11 and cre_dt > now()::time - INTERVAL '15 min';

--select * from txn_tok where de2 = 364711808 order by cre_dt desc
--select * from txn_tok (NOLOCK) where De11 = @11 and cre_dt > dateadd(minute, -5, getdate())
--select @2to
--select @txn_id
	if p2to > 0 then
	select p2 = ecno from p_tok  where pr_tok = p2to;

	END IF;
--select * from p_tok where ecno = 0x13D74EEA4AD3612C56D0AED5B7B3BEB5
END IF;
------------------------------------

if p2 >'\x00' then 

raise notice '%',p2;
   
  select   status_nwk, status_sys, pr_tok, p_tok.serv_code, TO_CHAR(expiry,'yyyymmdd'),expiry, pcode, addr, atc, pvv,npvv, p_tok.tok_flags, 
   DE38, pb_tok,pin_try_rem_onl, pin_try_rem_chp, p_product.client_id,p_product.prod_id,status_sys,acc_id,key_id,rules_flag,
   p_product.service_3ds_status,p_tok.pan_seq,p_tok.auth_type,enable_multi_acc_card,psd2_check,offline_pin_check,hsm_status,ccy_bill INTO pstat_nwk,pstat_sys,p2to,pserv_code, 
   temp_expiry,pexpiry_with_timestamp,pcpo,pcad,patc,ppvv,ppvv_hsm_generated, ptok_flags, p38i,ppb_tok, ppin_rem_onl, ppin_rem_chp, pclient_id,pprod_id,pstatus_sys,pacc_id,pkey_id,
   prls_applied,p3ds_status,ppan_seq ,pauth_type,penable_multi_acc_card,ppsd2_check,poffline_pin_check,_hsm_status,_prod_bill_ccy
   from p_tok  inner join p_product on  p_tok.crd_prdct_id = p_product.prod_id where ecno = p2 limit 1;  -- NOTE NOTE NOTE add status_nwk constraint
    raise notice 'p2to from without  expiry %',p2to;

 -- end if;
--Changes by @Abhishek for JETCO's OSQ transaction, because expiry is expected to be sent from PT.
pexp:=temp_expiry::int;
pact_exp:= substring(temp_expiry,3,4);

END IF ;
 
    prsn_cde :='000';
	
	----"-- Rohan 24Oct18 ms=>pg  unknown card part added"
	raise notice 'token % %',p2to,pprod_id;
	
	-- changes by Anil 06 sept 2019 (txn_checks)
	txn_checks_applied := txn_checks_applied | 16::bigint; -- BIT 5 unknown_card_repeat check

raise notice 'pstatus_sys %',pstatus_sys;  ---- ANIL CHECK

if pstatus_sys = 99  then  ----- Card recognised as a previously used unknown card (not one set up by client)

			IF pinstr ='00' then 
			pinstr :=''; 
			END IF;
			--pinstr := pinstr  +   'upb:' + to_char(int,ppb_tok) + ';uac:' + to_char(int,pacc_id)	+ ';' ;
			papprove_so_far=false;
			p39o := '14';
			prsn_cde := 132;
			pvalid_notes := pvalid_notes  || ': Unknown card (used previously). Placed in garbage account' ;
		
			-- unknow card repeated
			txn_checks_results := txn_checks_results | 16::bigint; -- BIT 5 unknown_card_repeat	
						
END if;
              raise notice 'ppin_rem_onl %',ppin_rem_onl;

	-- changes by Anil 06 sept 2019 (txn_checks)
	txn_checks_applied := txn_checks_applied | 8::bigint; -- BIT 4 unknown_card_new check
	
p2to =coalesce (p2to,0);
if  p2to =0   then 

		-- unknown card new
		txn_checks_results := txn_checks_results | 8::bigint; -- BIT 4 unknown_card_new

			--select @rowcount = 0
			--- Insert row to handle new UNKNOWN card
			
			Select  p_product.prod_id,  1199, 1199, '9999',0, 0,  0,  0, 0, tok_flags, 0,default_pbtok,0, 0, client_id, '000', default_acc,auth_type,rules_flag
			into pprod_id,pstat_nwk,pstat_sys,pact_exp,pcpo,pcad,patc,ppvv,ppvv_hsm_generated,ptok_flags,p38i,ppb_tok,ppin_rem_onl,ppin_rem_chp,pclient_id,pserv_code,pacc_id,pauth_type,prls_applied
			from  p_product  where   pbin::int  >=   LEFT(bin_lo || '00', 6)::int  and pbin::int <= LEFT(bin_hi || '99', 6)::int and card_type = '99'
			order by bin_hi-bin_lo  limit 1 ;
			raise notice 'Token and bin incmoing :  % pbin % product id %' ,p2to ,pbin,pprod_id;

			raise notice 'Unknown card: Placed in garbage account' ;
				
			prsn_cde := 132;
			papprove_so_far :=false;
			p39o := '14';
			pvalid_notes := pvalid_notes  || ': Unknown card: Placed in garbage account' ;
			pprod_id=coalesce(pprod_id,0);
			if pprod_id =0 then
			
			Select  p_product.prod_id,  1199, 1199, '9999',0, 0,  0, 0, 0,  tok_flags, 0,default_pbtok,0, 0, client_id, '000', default_acc,0,rules_flag
			into pprod_id,pstat_nwk,pstat_sys,pact_exp,pcpo,pcad,patc,ppvv,ppvv_hsm_generated,ptok_flags,p38i,ppb_tok,ppin_rem_onl,ppin_rem_chp,pclient_id,pserv_code,pacc_id,pauth_type,prls_applied
			from  p_product  where   prod_name = 'NEW BIN';
			prsn_cde := 133;
			papprove_so_far :=false;
			p39o := '14';
			pvalid_notes := pvalid_notes  || ': Unknown BIN: Placed in garbage account' ;
			
			
			END if;
			
			-------------------------------- Grab a token --------------------
			UPDATE pbtok set status=3 WHERE  pbtok = (  SELECT pbtok FROM pbtok where pbtok.status=1 LIMIT  1)  RETURNING pbtok into pdummy_pb_tok;
			UPDATE prtok set status=3 WHERE  prtok = (  SELECT prtok FROM prtok where prtok.status=1 LIMIT  1)  RETURNING prtok into pdummy_pr_tok;
			
			p2to := pdummy_pr_tok;

			--select @2to
			--- Select pr tok from stack
			INSERT INTO p_tok(pr_tok, pb_tok, expiry, serv_code, cre_dt, cre_by, status, last_txn_dt, kcv, pan_seq, ecno, card_type, 
			key_id, ecno_old, kid, kid_old, pcode,  atc, pvv, status_nwk, status_sys, acc_id, crd_prdct_id, tok_flags,auth_type)
			values (pdummy_pr_tok, pdummy_pb_tok,cast( '2017-01-01 00:00:00' as timestamp), '009', localtimestamp, 0, 0, localtimestamp, null, '00', p2, 0, 
			ppcnki, null, 0, 0, 99, 0, null, pstat_nwk, 99, pacc_id, pprod_id, ptok_flags,pauth_type);  --- Insert ecno into p_tok
			
		--	GET DIAGNOSTICS prowcount = ROW_COUNT;

			--select @rowcount = @@ROWCOUNT
			--if prowcount >0 then
			
			update pbtok set status=2 where pbtok = pdummy_pb_tok; --- updating status of the stack
			update prtok set status=2 where prtok = pdummy_pr_tok; --- updating status of the stack
			IF pinstr ='00' then pinstr :=''; END IF;
			--pinstr := pinstr  ||   'upb:' ||  to_char(int,pdummy_pb_tok)  || ';uac:' || to_char(int,pacc_id)	|| ';'  ;
			update pan_stack set status=99 where ecno=p2;
		--	END IF;

			--INSERT INTO p_tok_detail(pr_tok, req_id, card_manf, order_ref, order_status, prod_ref, carrier_type, carrier_design_ref, carrier_lang, carrier_insrt1, carrier_insrt2, card_design_ref, cust_pur_id, cust_ch_id, cust_delv_id, del_method, del_code, image_create, image_time, activate_date, emboss_name, emboss_line4, ext_line1, ext_line2, image_name, logo_front_id, logo_rear_id, sms_options, start_date, exp_date, crd_action, crd_instr, envelope_ref, trk3, cust_account, load_val, load_ccy, load_type, load_origin_fin, load_src, loaded_by, load_fee, load_fee_type, load_fee_ccy, external_auth, client_card_ref, bulk_delv_add_code, ord_status_note)
			--values(@dummy_pr_tok, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, getdate(), 'Suspect Fraud', 0, 0, 0, 0, 0, 0, 0, '01/17', '01/17', 0, '', 0, 0, 0, 0, '000', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);  --- Insert token detail into db

			--------------------------------------------------------------------
END if;
		
		
		if p2to =0   then
		--- Insert row to handle new UNKNOWN card
		
		Select  p_product.prod_id,  1199, 1199, '9999',0, 0,  0, 0, 0,  tok_flags, 0,default_pbtok,0, 0, client_id, '000', default_acc
			into pprod_id,pstat_nwk,pstat_sys,pact_exp,pcpo,pcad,patc,ppvv,ppvv_hsm_generated,ptok_flags,p38i,ppb_tok,ppin_rem_onl,ppin_rem_chp,pclient_id,pserv_code,pacc_id
			from  p_product  where   pbin  >=   LEFT(bin_lo || '00', 6)  and pbin <= LEFT(bin_hi || '99', 6) and card_type = 9;
			
			
-- 		Select top 1 @stat_nwk = 1199, @stat_sys = 1199, @2to = default_prtok, @act_exp = '9999',
-- 		@cpo = 0, @cad = 0, @atc = 0, @pvv = 0,  @prod_id= p_product.prod_id,
-- 		@tok_flags = tok_flags, @38i = 0,@pb_tok=default_pbtok,@pin_rem_onl= 0, @pin_rem_chp= 0, @client_id=client_id, @serv_code = '000'
-- 		from  p_product (NOLOCK) where   left(@bin,6)  >=   LEFT(bin_lo ,6 )  and left(@bin,6)  <= LEFT(bin_hi,6) and card_type = 9
		prsn_cde = 132;
		papprove_so_far=false;
		p39o = '14';
		pvalid_notes = pvalid_notes  || ': Unknown card Card outside all product bin range - nearest bin fit used ' ;
		IF pinstr ='00' then  pinstr ='' ; END IF;
		pinstr = pinstr  ||   '99:unknown;'	;
		END IF;

		if	p2to =0  then
		
		p2to = 9999;
		pact_exp = '9999';
		p39o = '14';
		prsn_cde = 111;
		pvalid_notes = pvalid_notes  || ': NO MATCHING CARD AND NO Holding Account/token' ;
		END IF;

--    if p2to is NULL then  
--       p2to := 9999;
--       pact_exp := '1299';
--       p39o := '14';
--       papprove_so_far := false;
--       pvalid_notes := coalesce(pvalid_notes,'')  || ': NO MATCHING CARD ';
--        prsn_cde := '111';
--    end if;

--       pvalid_notes := coalesce(pvalid_notes,'')  || ': incoming track '|| p35 || '     db serv code:' ||  pserv_code;
 
------------------------------ DEFINITION OF THE VALIDATIONS ------------------------

--First Extract DE3_1, DE22_1, DE61_10 and    DE48 SE61 (5SF)   store for checks:
-- store as @p + fieldid + _ + subfield id + _ + subsubfield id... eg De48 Subfield 61 subsubfield 2 would be @p48_61_2
-- so you will have DE3 fully broken down, De22 fully broken down, De48 fully broken down etc.

--NOTE FOR SANTHOSH - I think we should put in all the validations.. we will have a list of mandatory ones at start which will apply to all cards
-- then below them we will have a list that will be only relevant to some cards.. we will look up an auth_checks table for the card and get a list of tests back:
-- ie perform test 8,11,12,18,21 etc etc.. then in the list you execute those tests. We will develop this as we go.
-- set up @pnote as varchar(100)  - we will input this to a table later.

-- NOTE NOTE NOTE - PLease add date and notes in here are we advance this proc.
-- SOH 30 Oct 19:27 gmt ... entered mandatory test list below 1-7

--MANDATORY TESTS
--1. Expiry (ignore for now)
--2. CVV (ignore for now)
--3. DE48 Se38... if present then put this char into position 6 in DE38 in 0110 (done) SVasu
--4. if DE 48, SE 61, SF 2 = 1 and txcn is cashback: if @p3_1 = 09 ==> make @p39o = 87 , @pnote = 'Cashback request from terminal that does not support it => Decline' (done) SVasu
--5. FALLBACK FAIL... Fallback to magnetic stripe on hybrid cards ==> if @p22_1= 80 ==> @p39o= 05 , @pnote='De22=80 fallback => Decline' (done) SVasu
--6. Crypto fail... if @pAC=0 then do: @p48_74 = '50I', @p39o= 88, @pnote = 'Cryptographic Failure => Decline' 
--7. FALLBACK FAIL  fb to PAN manual entry on hybrid cards: if @p22_1 = 79 ==> @p39o= 05 , @pnote='De22=79 fallback => Decline' (done)SVasu

  -- SHANE TEMP work around
  -- pbin := '543712';
  

--------------------SETTING UP DEFAULT VALUE FOR P_TOK UPDATE ---------------------------------------
--ROH : 2020-11-09
--Assigning the value from p_tok for the update 
if p2to>0 then 

   pact_final=patc;
   ppin_rem_onl_final=ppin_rem_onl;
   ppin_rem_chip_final=ppin_rem_chp;
   pstatus_nwk_final=pstat_nwk;
   p38_final=p38i;
   ptok_flags_final=ptok_flags;
   ppvv_final=ppvv;
   ppvv_hsm_final =ppvv_hsm_generated;
end if;
 if ppvv is NULL then 
  	ppvv :=0;
 end if;
	
---------------------------------------------------------------------------------------------------

  
  
  --ROH : 2021-07-27 
  --HSM pvv is 2 byte and stored in npvv , if npvv is of length 2 byte then assign this 
  --value to the ppvv
  
  --ROH :2021-09-02 
  --_hsm_status if hsm is enable , check npvv , else check pvv
--=======================================-----------------------
	RAISE INFO 'New PVV generated by hsm %',ppvv_hsm_generated;
	--hsm_status  : Y = check npvv column, N: check pvv column
	IF _hsm_status IS NULL or _hsm_status='' THEN
		_hsm_status :='N';
	END IF;
	
	IF _hsm_status='Y' THEN 
		ppvv=ppvv_hsm_generated;
	END IF;
	
-- 	IF LENGTH(ppvv_hsm_generated)=2 THEN
--		ppvv=ppvv_hsm_generated;
-- 	ELSE
-- 		---User normal ppvv mode 
-- 			RAISE INFO 'Norm PVV generated by pks %',ppvv;

-- 	END IF;

---=======================================

----------------- SPLIT DE3 --------------------

   p3_1 := SUBSTR(p3,1,2);
   p3_2 := SUBSTR(p3,3,2);
   p3_3 := SUBSTR(p3,5,2);

------------------ SPLIT DE22 --------------------

   p22_1  := SUBSTR(p22,1,2);
   p22_2  := SUBSTR(p22,length(p22) -1+1);

------------------   SPLIT DE 48 ----------------
  raise notice 'table tt_PTMPSUBELEMENTS for DE48';
   raise notice 'de48 breaking';
   
   raise notice 'de48 breaking p approve %', papprove_so_far;  ---- ANIL CHECK

 	---ROH 20201111--remvoing temp table and using array 
--    BEGIN
--     drop table if exists tt_PTMPSUBELEMENTS;
--       CREATE TEMPORARY TABLE tt_PTMPSUBELEMENTS
--       (
--          Field_id INTEGER,
--          se_Val VARCHAR(4000)
--       );
--       exception when others then
--          truncate table tt_PTMPSUBELEMENTS;
--    END; 
/****Validation 3****/
--    33330101P0216542402000021999303042710710414C 

   pD48_S1 := SUBSTR(p48,1,1);
  if pmtid like '02%%' then
      if  SUBSTR(p48,1,1) ~ '^\d+(.\d+)?$' then
      --if isnumeric(SUBSTR(p48,1,1))  then
      pi = 1;
      else
      pD48_S1 := SUBSTR(p48,1,1);
      
	  end if; 
  end if;
  p48_san =SUBSTR(p48,1,1);
  --if SUBSTR(p48,2,2) <> '71' then
      if lower(pcn)<>'u'  then
            while (pi < LENGTH(p48)) LOOP
              pfid := CAST(SUBSTR(p48,pi,2) AS SMALLINT);
              pflen := CAST(SUBSTR(p48,pi+2,2) AS SMALLINT);
			   	---ROH 20201111--remvoing temp table and using array 
--              insert into tt_PTMPSUBELEMENTS(Field_id, se_val)
--              select pfid, SUBSTR(p48,pi+4,pflen);
               
			   se_val:= SUBSTRING(p48,pi+4,pflen);
  			   pde48_array[pfid]=se_val;			   
				
			  raise notice' DE48 subfield %',pfid;
			  	--replacing cvv2 value if present
			   if pfid='92' then se_val=repeat('*',pflen); end if;
			    p48_san:=concat(p48_san,pfid,SUBSTR(p48,pi+2,2),se_val);
				
				 pi := pi+pflen+4;
           END LOOP;
  end if;     
  
  raise notice 'Sanitize DE48 %',p48_san;

  ---ROH 20201111--remvoing temp table and using array  pde48_array

--    if exists(select 1  from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('61','') as INTEGER)) then
--       select   se_val INTO pDE048se61 from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('61','') as INTEGER);
--    end if;
  pDE048se61 =pde48_array[61];
  p48o  := p48;
  
-------------------------- SPLIT DE53 ---------------------------------
   if LENGTH(p53) > 0 then
      p53_1 := SUBSTR(p53,1,2);
      p53_2 := SUBSTR(p53,3,2);
      p53_3 := SUBSTR(p53,5,2);
      p53_4 := CAST(SUBSTR(p53,7,1) AS SMALLINT);
   end if;

-------------------------- SPLIT DE60 ---------------------------------

   if LENGTH(p60) > 0 then
      p60_1 := SUBSTR(p60,1,3);
      p60_2 := SUBSTR(p60,4,4);
      p60_3 := SUBSTR(p60,8,53);
   end if;

-------------------------- SPLIT DE120  (before DE61 because need AVS detail)---------------------------------

   if LENGTH(p120) > 5 then
      pdo_AVS_resp := true;   -- AVS reply should be given in 48_83 because address was provided in 120
      p120_id := SUBSTR(p120,1,2);
--select @p120_avs = Substring(@p120,5,40)
      if p120_id in('01','02','03','04') then
         p120_pc := replace(SUBSTR(p120,5,9),' ','');
         p120_addr := replace(SUBSTR(p120,14,20),' ','');
      end if;
   end if;

-------------------------- SPLIT DE61 ---------------------------------

   if LENGTH(p61) > 2 then
      p61_1 := SUBSTR(p61,1,1);
      p61_2 := SUBSTR(p61,2,1);
      p61_3 := SUBSTR(p61,3,1);
      p61_4 := SUBSTR(p61,4,1);
      p61_5 := SUBSTR(p61,5,1);
      p61_6 := SUBSTR(p61,6,1);
      p61_7 := SUBSTR(p61,7,1);
      p61_8 := SUBSTR(p61,8,1);
      p61_9 := SUBSTR(p61,9,1);
      p61_10 := SUBSTR(p61,10,1);
      p61_11 := SUBSTR(p61,11,1);
      p61_12 := SUBSTR(p61,12,2);
      p61_13 := SUBSTR(p61,14,3);
      p61_14 := SUBSTR(p61,17,10);
   end if;

		--ROH : 2021-09-19 checking ccard status 
---------------------Checking if the card network status -------------
			IF pstat_nwk <>1000 THEN
			---card is blocked , declining transaction 
				papprove_so_far =false;
				p39o :='05';
			END IF;

--------------------------------------------------
     ---------------- Check first Switches ---------------------------------

 ---------------------------------- TOK FLAGS SETTINGS --------------------------  Decimal val---
--  00000000 00000000 00000000 00000000 
--pin_new   1		generate new rand pin and send to card on nxt EMV txn   2		1073741824
--send_bal   1		Send card bal in De54 in next auth						3		536870912
--pin_ub   1		Send Pin unblock to card								4		268435456
--Blk_ecom   1		Sets card to decline all Ecommerce						9		8388608
--Blk_atm   1		Sets card to decline all ATM							10		4194304
--Blk_bal_enq  1	Sets card to deline Bal enquiry over auth				11		2097152
--Blk_csh_bak  1	Sets card to decline cash back txns						12		1048576
--Blk_csh_disb  1	Sets card to decline De3_1 = 17							13		524288
--Blk_cr_auth  1	Sets card to decline credit pur/refund (DE3_1=20)		14		262144
--Blk_cr_auth  1	Sets card to decline credit deposit (DE3_1=21)			15		131072
--Blk_cr_auth  1	Sets card to decline credit adjust (DE3_1=22)			16		65536
--Blk_cr_auth  1	Sets card to decline chq deposit guarantee (DE3_1=23)   17		32768
--Blk_cr_auth  1	Sets card to decline chq deposit (DE3_1=24)				18		16384
--Blk_cr_auth  1	Sets card to decline credit payment (DE3_1=28)			19		8192
--Blk_cr_auth  1	Sets card to decline non base currency txn				20		4096
--Blk_acc_enq  1	Sets card to decline all account enq (de61_7=8)			21		2048	
--Blk_crd_np  1		sets card to decline Card not present txns				22		1024
--Blk_ch_np   1		sets card to decline Cardholder not present txns		23		512
--Blk_fo_cm_atm  1	sets card to decline failover to mag at emv capable atm 24		256
--Blk_mag_atm  1	sets card to decline mag at atm							25		128
--Alow_fallbk_cac 1 Alow if fallback chp->mag at chp capable pos DE22_1=80  26		64
--Alow_mag_atm  1	Allow if de22_1='02' & De48_88 ='Y'						27		32 (compliance downgrade - acq not qualified to submit mag)
--Alow_AVS_Fail     Sets card to approve if AVS match fails					28		16 
--Ext_Auth_OFF  1	Set card to External Auth OFF							32		1

  ---------------- Check first Switches ---------------------------------
 ----ROH : 2020-04-19 Adding MDES check 
---ROH 20201111--remvoing temp table and using array  pde48_array
-- select   se_val INTO pd48_26 from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('26','') as INTEGER);
  
  pd48_26=pde48_array[26];
  raise notice'pd48_26 %',pd48_26;

--   select   se_val INTO pd48_71 from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('71','') as INTEGER);
--  raise notice '  pd48_71 %',pd48_71;
 
--   if pd48_71<>'' then
--    pd48_71ifid=4;
--    raise notice 'pd48_71 %',pd48_71;

--    while (pd48_71i < LENGTH(pd48_71)) LOOP
--   pd48_71_sf1 = Substr(SUBSTR(pd48_71,pd48_71i,pd48_71ifid),1,2);
--   pd48_71_sf2 = Substr(SUBSTR(pd48_71,pd48_71i,pd48_71ifid),3,1);
--   pd48_71i=pd48_71i+pd48_71ifid;
  
--   if pd48_71_sf1='61' and then
  
--    raise notice '  pd48_71 % ,pd48_71_sf1 % , pd48_71_sf2 %',pd48_71,pd48_71_sf1,pd48_71_sf2;

--   end loop;

--  end if;
 
if papprove_so_far is true then

	txn_checks_applied := txn_checks_applied | (2^35)::bigint; -- BIT 36 mag_from_chip_fallback check
---ROH 20201111--remvoing temp table and using array  pde48_array
 --if p22_1 = '02' and exists(select 1  from tt_PTMPSUBELEMENTS where Field_id = 88) and substring(ptok_flags,27,1) =0::bit then 
 if p22_1 = '02' and pde48_array[88] is not null and substring(ptok_flags,27,1) =0::bit then 
		 p39o := '05';
		 papprove_so_far := false;
		  prsn_cde := '210';
		 pvalid_notes := coalesce(pvalid_notes,'')  || ': acq not qual for mag - decline ';
	 -- end if;
	 
	 -- changes by Anil 06 sept 2018 (txn_checks)
	 elseif p22_1 = '81' then
	 	-- ecommerce transaction
		txn_checks_applied := txn_checks_applied | 64::bigint; -- BIT 7 card_flag_ecommerce check
		
	 	if substring(ptok_flags,9,1) = 1::bit  then --bit 24 counting from left  then
		  p39o = '05';
		  papprove_so_far = false;
		  prsn_cde = '500';
		  pvalid_notes = pvalid_notes ||  ': Token flags: Decline E-commerce flag set to 1 ';
		  -- declined
		  txn_checks_results := txn_checks_results | 64::bigint; -- BIT 7 card_flag_ecommerce_decline
 		end if;

	elseif p22_1 = '79' or p22_1 = '80' then
		txn_checks_results := txn_checks_results | (2^35)::bigint; -- BIT 36 mag_from_chip_fallback

	 else 
			-- non ecommerce
			txn_checks_applied := txn_checks_applied | 72057594037927936::bigint; -- BIT 57 card_flag_NON_ecommerce check
			
			if length(pd48_26)>2 then
			 raise notice 'checking mdes';
    		 	txn_checks_applied := txn_checks_applied | 288230376151711744::bigint; -- BIT 59 MDES transaction failed check 

				if substring(ptok_flags,5,1) =1::bit and  pd48_26 NOT in('103','216','217','327')then --bit 27 counting from left  then
				  p39o = '05';
				  papprove_so_far = false;
				  prsn_cde = '500';
				  pvalid_notes = pvalid_notes ||  ': Token Flags: Decline All non-MDES( and non-ecom) card flag set to True ';
				 --decline
				  txn_checks_results := txn_checks_results | 72057594037927936::bigint; -- BIT 57 card_flag_NON_ecommerce_decline
				-- added by Anil 22 oct 2020
				  txn_checks_results := txn_checks_results | 288230376151711744::bigint; -- BIT 59 		mdes transation failed
				end if;
			  
			  else
			  
				  if substring(ptok_flags,5,1) =1::bit and  (p22_1 NOT in ('00','01')) and p61_4  NOT in ('4','5') then --bit 27 counting from left  then
				  p39o = '05';
				  papprove_so_far = false;
				  prsn_cde = '500';
				  pvalid_notes = pvalid_notes ||  ': Token Flags: Decline All non-Ecom( and non-MDES) card flag set to True ';
				  --decline
				  txn_checks_results := txn_checks_results | 72057594037927936::bigint; -- BIT 57 card_flag_NON_ecommerce_decline
				
				end if;	
			end if;
		
	end if; 	
	
	
	----Jan 09,2020
	----Decline all contactless transaction 
	raise notice 'checking contactless transaction p22_1% , tokflag %',p22_1,substring(ptok_flags,6,1);
	if substring(ptok_flags,6,1) =1::bit and  (p22_1  in ('07','91')) then
			  p39o = '05';
			  papprove_so_far = false;
			  
			  pvalid_notes = pvalid_notes ||  ': Token flags: Decline all Contactless flag set to 1 ';
	end if;
	
--JAN 12 2020,ROHAN	
--Sets card to decline all ATM Cash bit position 10  de3_1=01
if substring(ptok_flags,10,1) =1::bit and  p3_1 ='01' then
			  p39o = '05';
			  papprove_so_far = false;
			  
			  pvalid_notes = pvalid_notes ||  ': Token flags: Decline all ATM Cash flag set to 1 ';
 end if;
--Sets card to deline Bal enquiry over auth bit position 11
if substring(ptok_flags,11,1) =1::bit and  p3_1 ='30' then
			  p39o = '05';
			  papprove_so_far = false;			  
			  pvalid_notes = pvalid_notes ||  ': Token flags: Decline Bal enquiry flag set to 1 ';
 end if;
-- ==============================================================
-- so DE22_1 = 79 or 80 ==> this is for this bit ==>
-- "sets card to decline failover to mag at emv capable atm  -bit position 24"
-- ==============================================================
if substring(ptok_flags,24,1) =1::bit and ( p22_1 ='79' or  p22_1 ='80') then
			  p39o = '05';
			  papprove_so_far = false;			  
			  pvalid_notes = pvalid_notes ||  ': Token flags: Decline failover to mag at emv capable atm  flag set to 1 ';
 end if;

-- DE22_1 in ('02','90,'91')    AND De3_1 = '01'    is the test for flag: ==>
-- 'sets card to decline mag at atm  -bit position 25'
if substring(ptok_flags,25,1) =1::bit and ( p22_1 ='02' or  p22_1 ='90' or  p22_1 ='91') then
			  p39o = '05';
			  papprove_so_far = false;			  
			  pvalid_notes = pvalid_notes ||  ': Token flags: Decline mag at atm flag set to 1 ';
 end if;
 
--  sets card to decline Card not present txns  -bit position 22
-- ==>decline if  De61_5 = 1
 if substring(ptok_flags,22,1) =1::bit and p61_4 ='1' then
			  p39o = '05';
			  papprove_so_far = false;			  
			  pvalid_notes = pvalid_notes ||  ': Token flags: Decline Card not present txns flag set to 1 ';
 end if;
 
--  decline if CArdholder not present (excluding ecommerce and recurring)  -bit position 23
-- ==> decline if de61_4 in (1,2,3)
 if substring(ptok_flags,23,1) =1::bit and (p61_4='1' or p61_4='2' or p61_4='3') then
			  p39o = '05';
			  papprove_so_far = false;			  
			  pvalid_notes = pvalid_notes ||  ': Token flags: DeclineCArdholder not present (excluding ecommerce and recurring) txns flag set to 1 ';
 end if;
 
-- decline if transaction is standing order or recurring   -bit position 29
-- ==> decline if De61_4 = 4  	
if substring(ptok_flags,29,1) =1::bit and p61_4 = '4' then
			  p39o = '05';
			  papprove_so_far = false;			  
			  pvalid_notes = pvalid_notes ||  ': Token flags: Decline standing order or recurring  txns flag set to 1 ';
 end if;
 
 --Blk_cr_auth  1	Sets card to decline non base currency txn				20		4096
	 if substring(ptok_flags,20,1) =1::bit and p49<> _prod_bill_ccy then 
		p39o = '05';
		papprove_so_far = false;			  
		pvalid_notes = pvalid_notes ||  ': Token flags: Sets card to decline non base currency txn flag set to 1 ';
	END IF;	
END IF;
  ------------------------ EXPIRY AND TRACK CHECKS ----------------------------------
raise notice 'p14 %',p14;

   --DS get product type 
   select product_type,enable_multi_acc_card,stand_in into _product_type,penable_multi_acc_card,pstand_in from p_product  where prod_id = pprod_id;
 
   raise notice 'pprod_id  %  _product_type  % at time %',pprod_id,_product_type ,  timeofday();
   select pad_value into ppad_value from p_padding  where prod_id  = pprod_id  
   and (p18 BETWEEN  mcc_lo and mcc_hi)  and (ccy = p49 or ccy = '000') 
   order by pad_value desc limit 1;
   
   
   
   

-------------STAND - IN - CHECK --------------------------------------------
 	
	raise notice 'Checking Stand-in Enable for this product at time %',  timeofday();
   if pstand_in=1 and pis_stip =true and (pinstr like '%timeout%' or pinstr like '%FAST:FAIL%') then
   	--changing to stand-in 
	  raise notice 'Stand-in Enable for this product';
	  pauth_type =0; 
	  --overriding the response code w.r.t to the rules engine response
	  pde39_nwk_out :=pde39_post_rules;
	  if pde39_nwk_out in ('00','08','10','87','85') then
	  	papprove_so_far=true;
		pde39_post_fast=91;
	  end if; 
   end if ; 

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--soh added 02 Jun 2020 to ensure balance adjust ONLY in approve situation.
if   papprove_so_far = true then
	--ROH : 2020-12-1 assignning value p6d to pfinal_total_debit 
		pfinal_total_debit=p6d;
---ROH : 2020-12-1 Commenting this block value assigned on above 
--   if substring(p3_1,1,1) = '2' then
--    -- SOH 03_dec_2019 remove +p28db
--    pfinal_total_debit := -(p6d);
--    else
--    -- SOH 03_dec_2019 remove +p28db
--    pfinal_total_debit := p6d;
--   end if;
end if;
-----------------------------------------------------------------------------

   
---ROH 2020-09-18 
-------------------------MULTI-ACCOUNT CHECK --------------------------------------------------
if penable_multi_acc_card='y' and pprod_id<>486 then 
    raise notice 'Multi Account Card Checking for token % de49 % de51 % at time %',p2ti,p49,p51,  timeofday();
   --select * from p_acc_tok limit 10
    drop table IF EXISTS temp_p_acc_tok CASCADE;
    CREATE TEMP TABLE temp_p_acc_tok(
       pr_tok bigint,ac_id bigint,currency char(3),is_default smallint,active_currency smallint
    );
    insert into temp_p_acc_tok(pr_tok,ac_id,currency,is_default,active_currency )
    select pr_tok,ac_id,currency,is_default,active_currency from p_acc_tok where pr_tok=p2ti;
   -- -- -- If (Transaction Currency DE49)  = (Supported wallet currency) 
   -- -- -- Then{
   -- -- -- Deduct Transaction amount(DE4) from corresponding wallet
   -- -- -- }
   -- -- -- Else if Billing amount(DE51)=IS_DEFAULT  {
   -- -- -- Deduct Billing amount(DE6) from corresponding currency wallet
   -- -- -- }
   ---------else { 
  ----------reject the transaction
  -----------  }select * From p_acc_tok where pr_tok=377668845

      select ac_id into pmulti_account from temp_p_acc_tok where pr_tok=p2ti and currency=p49;
      if pmulti_account is null then 
          --supported wallet not found 
         raise notice 'No supported wallet found';
         select ac_id into pmulti_account from temp_p_acc_tok where pr_tok=p2ti and currency=p51;-- and is_default=1;
            raise notice 'pmulti_account % at time %',pmulti_account,  timeofday();           
             if pmulti_account is null then
               raise notice 'Default account didnt matched with the billing currency';
               -- pDE39_nwk_out= '05 ';
               papprove_so_far =false ;
               pauth_note = pauth_note || 'Multi-Account checked failed :ACC Not found';
               ---to-do need to remove this later .
               --Temporary fix      
            else ---corresponding default account found 
               raise notice 'Corresponding default acccount found';
            end if;
    else 
        raise notice 'Supported Wallet Found at time % ',  timeofday();
       --supported wallet  found 
       --deducting amount from the supported wallet 
          p6d := p4d;
		  if papprove_so_far is true then pfinal_total_debit=p4d;end if;
          raise notice 'pmulti_account transaction amount  %',p6d;
      ---Getting account balance details   
    end if;
 else  ---Multi account check else part
 ----Multi-account not supported , getting default account by token
     raise notice 'Multi Account not supported , getting default account by token';
 end if;---Top multi currency check 

-----Selecting Main balance from the account 
--SOH added 17 Jul 2021 ... remove all p_accounts activity if pfinal_total_debit = 0
-- if pfinal_total_debit <>0 then   -- commenting out the addition made by SOH, because it wouldn't populate the accounts details hence the accounts details for this card was seen all zeros in paycontrol.

	if pmulti_account is null or penable_multi_acc_card <>'y'then 
   	---Getting account from default currency
   
  	 pauth_note = pauth_note || 'default Acc used';
   	Select ac_id,   pb_tok,  tok_flags,p_accounts.bill_ccy, bill_act_amt, 
   	bill_blk_amt,   bill_pnd_amt
   	into pv_acc_id,ppb_tok,ptok_flags,pnccy,pac_act_bal,pac_blk_amt,pac_pnd_amt from toks  inner join p_accounts  on toks.acc_id = p_accounts.ac_id
   	WHERE tok = p2ti and active=1::boolean;
   	raise notice 'balance of the account % at time %',pac_act_bal+(pac_blk_amt+pac_pnd_amt),  timeofday();
   	raise notice 'pnbal %',pnbal;

	else 
   		pauth_note = pauth_note || 'Multi-cccy Acc used';
  		raise notice 'Selected pmulti_account %',pmulti_account;
	
		Select p_accounts.ac_id,   pb_tok,  tok_flags,p_accounts.bill_ccy, bill_act_amt, 
         	bill_blk_amt,   bill_pnd_amt
         	into pv_acc_id,ppb_tok,ptok_flags,pnccy,pac_act_bal,pac_blk_amt,pac_pnd_amt 
         	from p_acc_tok  
         	inner join p_accounts  on p_acc_tok.ac_id = p_accounts.ac_id
         	inner join toks  on toks.tok = p_acc_tok.pr_tok
         WHERE pr_tok = p2ti and p_accounts.ac_id=pmulti_account;
         raise notice 'balance of select multi-account % at time %',pac_act_bal+(pac_blk_amt+pac_pnd_amt),  timeofday();
         raise notice 'pnbal %',pnbal;
	end if;
-- end if;
raise notice 'pAC %', pAC;  ---- ANIL CHECK

	-- SOH added 29th March 2020   Pan_seq check... mainly for replacement card - to prevent use of old card (esp if expiry not provided in 0100)
	txn_checks_applied := txn_checks_applied | 128::bigint; -- BIT 8 has card expired

  if Right(ppan_seq,2) > right(p23,2)   and papprove_so_far = true then
		papprove_so_far = false;
		--ROH --2020-08-20 , Change 54 -->14 for the expiry not matched case
		p39o:='14';
		prsn_cde :='258';
		pvalid_notes := pvalid_notes || ':Expired card (pan seq check)'  || '...' || pact_exp ;
		--txn_checks_results := txn_checks_results | 128::bigint; -- BIT 8 has card expired
		txn_checks_results := txn_checks_results | (2^60)::bigint; -- bit 60 Card Expiry Validation

	end if;
		
	
	-- SOH added 28th March 2020
	if pact_exp < TO_CHAR(current_date,'yymm') and papprove_so_far = true   then
		papprove_so_far = false;
		p39o:='54';
		prsn_cde :='258';
		pvalid_notes := pvalid_notes || ':Expired card'  || '...' || pact_exp ;
		--txn_checks_results := txn_checks_results | 128::bigint; -- BIT 8 has card expired
		txn_checks_results := txn_checks_results | (2^60)::bigint; -- BIT 60 Card Expiry Validation

	end if;
	
	
	-- ROH added 2021-08-12
	--Checking expiry with yyyymmddss
	IF pexpiry_with_timestamp IS NULL THEN 
		pexpiry_with_timestamp :='1999-01-01'::timestamp ;--setting up old expiry date in case of null expiry which we will receieve for unknow card
	END IF;
	
	IF pexpiry_with_timestamp < to_timestamp(p7o, 'YYYYMMDDHH24MISS')  and papprove_so_far = true   then
		papprove_so_far = false;
		p39o:='54';
		prsn_cde :='258';
		pvalid_notes := pvalid_notes || ':Expired card'  || '...' || pact_exp ;
		--txn_checks_results := txn_checks_results | 128::bigint; -- BIT 8 has card expired
		txn_checks_results := txn_checks_results | (2^60)::bigint; -- BIT 60 Card Expiry Validation

	end if;
	
if p14<>'0000' 	or p14 is not null	 then ---no de14 
-- changes by Anil 06 sept 2019 (txn_checks)
txn_checks_applied := txn_checks_applied | 256::bigint; -- BIT 9 expiry_check_db check

	if pact_exp <> p14  and papprove_so_far = true then --- Expiry check
	--ROH : Card expiry changed to 54 from 05 on 2020-08-13
	--ROH --2020-08-20 , Change 54 -->14 for the expiry not matched case
	p39o = '14';
	papprove_so_far=0;
	p38o = NULL;
	prsn_cde = '258';
	pvalid_notes = pvalid_notes  || ': Expiry in Auth mismatch to card expiry ';

	-- mismatch
	txn_checks_results := txn_checks_results | 256::bigint; -- BIT 9 expiry_check_db_mismatch
END IF; ---expiry check
END if ;
--pat_in  = p55_9f36::int;
 if pAC =1  then 
--update toks set atc = @at_in where  tok = @2to 
   --  pupdate_tok = pupdate_tok  ||  ', atc = ' || pat_in::varchar;   -- means  update toks set atc = pat_in where  tok = p2to;
   --ROH 2020-11-09 Change in the update p_tok 
   pact_final=pat_in;
else
   --ROH 2020-11-09 Change in the update p_tok 
   --updating same atc from p_tok table 
   pact_final=patc;

 ------------ MAG STRIPE TRANSACTION ------------
 raise notice 'Expriy and Track checks ';

--Mag stripe checks:
--A = Track 1 or Track 2 not present in the message
--B = Track 1 and Track 2 present in the message
--C = DE 2 (Primary Account Number [PAN]) not equal in track data
--D = DE 14 (Expiration Date) not equal in track data
--E = Service code invalid in track data
--F = Field separator(s) invalid in track data
--G = A field within the track data has an invalid length
--H = DE 22 (Point-of-Service [POS] Entry Mode), subfield 1 (POS Terminal PAN Entry Mode) is
--80, 90, or 91 when DE 48, Transaction Category Code (TCC) is T
--I = DE 61 (Point-of-Service Data), subfield 4 (POS Cardholder Presence) is 1, 2, 3, 4, or 5
--J = DE 61 Point-of-Service Data), subfield 5 (POS Card Presence ) is 1

	if papprove_so_far is true then 
	
		if pmtid = '0100' or pmtid = '0200' then

			raise notice 'Expriy and Track checks  for mtid %',pmtid;

			
			if p35 is NUll and p45 is Null ANd p3_1 in  ('01','91','92')    and papprove_so_far = true then --- no track
			--ROH 2020-11-22 commenting this assigning integer in bytea gives error
			--pp_info is not used so safe to comment this out
			--pp_info = pP_info || 16;
			p48o= p48o || '8901A';
			p39o = '57';
			papprove_so_far=0;
			prsn_cde = '250';
			p38o = NULL;
			pvalid_notes = pvalid_notes  || ': no track data ';
			raise notice 'no track data';
			else
			

			if length(p35)>0  and length(p45)>0    and papprove_so_far = true   --- both tracks
				then 
				--ROH 2020-11-22 commenting this assigning integer in bytea gives error
				--pp_info is not used so safe to comment this out
				--pp_info = pP_info || 32;
				p48o= p48o || '8901B';
				p39o = '57';
				prsn_cde = '252';
				papprove_so_far=0;
			
				p38o = NULL;
				pvalid_notes = pvalid_notes || ': 2 tracks provided ';

			
			ELSE
				raise notice 'Else part Expriy and Track checks  for track data';

				if pact_exp = '9999'   and papprove_so_far = true  then  ---no card found check
						p39o = '14';
						papprove_so_far=0;
						p38o = NULL;
						prsn_cde = '254';
						pvalid_notes = pvalid_notes  || ': No Card Found ';

				ELSE
				if p14='0000' 		 then ---no de14 
				 	pvalid_notes = pvalid_notes  || ': NO DE14 ';
								--set @14=substring(@35,3,4) 
				ELSE 

					-- changes by Anil 06 sept 2019 (txn_checks)
							txn_checks_applied := txn_checks_applied | 256::bigint; -- BIT 9 expiry_check_db check
						
					if pact_exp <> p14  and papprove_so_far = true then --- Expiry check
					--ROH --2020-08-20 , Change 54 -->14 for the expiry not matched case
				    p39o = '14';
					papprove_so_far=0;
					p38o = NULL;
					prsn_cde = '258';
					pvalid_notes = pvalid_notes  || ': Expiry check fail ';
					
					-- mismatch
					txn_checks_results := txn_checks_results | 256::bigint; -- BIT 9 expiry_check_db_mismatch
				END IF; ---expiry check
				
					-- chanages by Anil 06 sept 2019 ( txn_checks)
							txn_checks_applied := txn_checks_applied | 128::bigint; -- BIT 8 expiry_check_trk_to_DE14 check
					
				if substring(p35,3,4) <> p14  and length(p14)>1 and Left(p35,1) <>'0'  and papprove_so_far = true  then  --is not NUll  --- Expiry check  -- substring(@35,18,4) (old.. based on full de35)
						-- New abbreviated DE35==>  ist Digit:  0 = no de35  1 = pan matching  2 = pan in de2 and de35 not matching
					    p48o= p48o || '8901D';
						--ROH --2020-08-20 , Change 57 -->14 for the expiry not matched case
						p39o = '14';
						p38o = NULL;
						prsn_cde = '256';
						pvalid_notes = pvalid_notes || ': De14 expiry not matched tr2 ' ;
					
						-- not matched
								txn_checks_results := txn_checks_results | 128::bigint; -- BIT 8 expiry_check_trk_to_DE14_mismatch
					END if; --is not NUll  --- Expiry check  
				END IF;---no de14 		
				END if ;		---no card found check
			end if;	--end  of p35 check --- both tracks			
			end if ;--end  of p35 check --- no track
		end if;--mtid check end
	END if; --end of top papprove_so_far
END IF;---END of  pAC =1 
 
  raise notice 'ppvv %',ppvv;
  

  --------------------- END EXPIRY AND TRACK CHECKS -----------------------------------

 -----------Roh this part to remove after full transalation------------------------------
 
------------------------ PIN TRIES CHECK ----------------------------------------
---Septmeber 18 --Roh removed this as per Shane 
--pin_rem_onl, pin_rem_chp 
-- 	if pin_rem_onl <0 or pin_rem_chp <0 then
-- 			 p39o := '75';
-- 			 p38o := Null;
-- 			 prsn_cde := '106';
-- 			 pvalid_notes := coalesce(pvalid_notes,'')  || ': Pin tries exceeded De39=75';
-- 			 papprove_so_far := false;

-- 	end if;
 
 --------------------------------------
 
 ----------------------- PVV CHECK -----------------------------------------------
raise notice 'PVV check started %', papprove_so_far ;  ---- ANIL CHECK

   
--to review once 
--pin check 
--pin unblock
--pin change
--pin counter 

if papprove_so_far is true then 
	--poffline_pin_check  ROH:2021-06-25
--- SOH feb 19 revisions to pin tries declines ---
--moved this logic below after NEW PIN MANAGEMENT
-- 	if poffline_pin_check=1 then ---check offline pin -- is NULL then
-- 		if (ppin_rem_chp<1 or ppin_rem_onl <1)  and papprove_so_far = true  then -- and (@3_1 <> '91' or @tok_flags & 268435456 =268435456)		then     
-- 				 p39o = '05';
-- 				 p38o = Null;
-- 				 prsn_cde = '225';
-- 				 if ppin_rem_chp<1 then
-- 				 	pvalid_notes = pvalid_notes || ': Chip pin tries exceeded via previous attempts';
-- 				 end if;	
-- 				 if ppin_rem_onl<1 then
-- 				 pvalid_notes = pvalid_notes || ': Online pin tries exceeded via previous attempts';
-- 				 END IF;
-- 				  papprove_so_far = 0;

-- 		END if;
-- 	END if;
-- changes by Anil 9 sept 2019 (txn_checks)
	--txn_checks_applied := txn_checks_applied  | 4096::bigint; -- BIT 13 pin_change_new_pin check
	
	---ROH-2021-06-25 --commenting this because pvv from hsm is in 2 byte length
-- 	if octet_length(ppvv_new) < 6 and p3_1 = '92' then
-- 		 p39o := '05';
-- 		 p38o := Null;
-- 		 prsn_cde := '226';
-- 		 pvalid_notes := coalesce(pvalid_notes,'')  || ': Pin change request - Bad new Pin (DE125)';
-- 		 papprove_so_far := false;
-- 			 -- bad pin new
-- 		 txn_checks_results := txn_checks_results | 4096::bigint; -- BIT 13 pin_change_bad_new_pin
-- 	end if;

	 -- changes by Anil 09 sept 2019 (txn_checks)
--	txn_checks_applied := txn_checks_applied | 8192::bigint; -- BIT 14 pin_change_bad_old_pin check

------Incorrect Onlin PIN CHECK --------------------------------------------------
raise notice 'incoming computed pvv %',p52;
raise notice 'stored  pvv %',ppvv;
	 if ppvv <> p52 and octet_length(p52) > 1 then
		if octet_length(ppvv_new) > 3 then --AND  @p22_1 = '05' AND  @p61_10 = '1'
			 p39o := '89';
			 p38o := Null;
			 prsn_cde := '201';
			 pvalid_notes := coalesce(pvalid_notes,'')  || ': Pin mismatch in a pin change request De39=89';
			 papprove_so_far := false;
			 txn_checks_results := txn_checks_results | 8192::bigint; -- BIT 14 pin_change_bad_old_pin
		  else
			 p39o := '55';
			 p38o := Null;
			 papprove_so_far := false;
			  prsn_cde := '117';
			  pvalid_notes := coalesce(pvalid_notes,'')  || ': Pin mismatch-de39=' || coalesce(p39o,'');
               if ppin_rem_onl > 0 then 
					 ppin_rem_onl = ppin_rem_onl -1;
					-- pupdate_tok = pupdate_tok  || ', pin_try_rem_onl = pin_try_rem_onl-1 ';  -- means  update p_tok set pin_try_rem_onl = pin_try_rem_onl-1 where pr_tok = p2to ;

					 --ROH : 2020-11-09 removing dynamice p_tok update 
					 ppin_rem_onl_final=ppin_rem_onl;
				end if;
			 
		 -- changes by Anil 06 sept 2019 (txn_checks)
			 txn_checks_applied := txn_checks_applied | 512::bigint; -- BIT 10 pin_tries_onl check
			 txn_checks_applied := txn_checks_applied | 2048::bigint; -- BIT 12 pin_tries_block_card check
			   raise notice 'ppin_rem_onl %',ppin_rem_onl;
			 if ppin_rem_onl <1 then
    	 			 p39o := '75';

					--pupdate_tok = pupdate_tok || ', status_nwk = 1005 ';  -- means  update p_tok set pin_try_rem_onl = pin_try_rem_onl-1 where pr_tok = p2to ;
					pvalid_notes = pvalid_notes  || ': Card blocked (1005) due to pin tries exceeded ';
					prsn_cde = '227';
					--pstat_nwk =1005;
					
					--ROH 2020-11-09 updating the variable to update on p_tok table 
					pstatus_nwk_final=1005;
					-- exceeded
					txn_checks_results := txn_checks_results | 512::bigint; -- BIT 10 pin_tries_onl_exceeded
					txn_checks_results := txn_checks_results | 2048::bigint; -- BIT 12 pin_tries_block_card
			END if;	---online pin counter end	 
		 
		  end if;--end of pin change check 
	   end if;--end of pvv<>p52	   
------Incorrect Onlin PIN CHECK END --------------------------------------------------	   

	   
 -- changes by Anil 06 sept 2019 (txn_checks)	 
	  --checking if de52 (pin)is avaialble or not in the transaction 
	  if  octet_length(p52) > 1 then 
	  	  	txn_checks_applied := txn_checks_applied | 2::bigint; -- online_pin check
	  if p52 = ppvv then
		   pvalid_notes := coalesce(pvalid_notes,'')  || ': Pin Matched';
	  else
		----pin mismatch ..checking further pin counter  
	  	txn_checks_results := txn_checks_results | 2::bigint; -- online_pin_mismatch
	  ------------------------ PIN TRIES CHECK ----------------------------------------
			
			---Sep 18,2019 , CHanged accrodign to Shane
			--offline pin counter check 
			if poffline_pin_check=1 then --check offline pin counter
				if (ppin_rem_chp <1)  then 
			 		 if pstat_nwk='1005' then 
				 		 p39o =  '05';
						 p38o = Null;
						 prsn_cde = '400';
						 pvalid_notes = pvalid_notes || ': Nwk_status override :to force decline De39=05';
						 papprove_so_far = 0;
				 	else 
						 p39o =  '75';
						 p38o = Null;
						 prsn_cde = '106';
						 pvalid_notes = pvalid_notes || ': Offline Pin tries exceeded De39=75';
						 papprove_so_far = 0;
					end if;
				end if;
			end if;--end of offline pin counter check	
			
			---online pin counter check 
			if (ppin_rem_onl <1 )  then 
					if pstat_nwk='1005' then 
						 p39o =  '05';
						 p38o = Null;
						 prsn_cde = '400';
						 pvalid_notes = pvalid_notes || ': Nwk_status override :to force decline De39=05';
						 papprove_so_far = 0;
				 	else 
				 
						 p39o =  '75';
						 p38o = Null;
						 prsn_cde = '107';
						 pvalid_notes = pvalid_notes || ': Online Pin tries exceeded De39=75';
						papprove_so_far = 0;
					end if;
			end if;			
	   end if;  --p52 = ppvv
   end if; --pin length check 
END if ; --top PVV papprove check 
raise notice 'PVV check completed';

----------------------- PVV CHECK Completed -----------------------------------------------

-------------- Account Status Enquiry Check ---------------------------
if papprove_so_far is true then 
if p61_7  = '8' then

    --At a minimum, issuers must validate that the account is valid and the
    --account is not listed on the Electronic Warning Bulletin (EWB).

    -- AVS Handling: @p48_82 = 52 ==> AVS check required  put reply in @p48_83

/*
Issuers must be able to receive AVS requests containing DE 48, subelement
82, value 52 and to respond with the appropriate value in DE 48,
subelement 83 (Address Verification Service Response).

Issuers must be able to receive CVC 2 requests in DE 48, subelement 92 and
to respond with the appropriate value in DE 48, subelement 87.
• The issuer, at its discretion, will send the acquirer an Authorization Request
Response/0110 message where DE 39 may contain either the value 00
(Approved), 05 (Do not honor), 85 (Not declined), or other valid business
declines. Examples of other acceptable business declines include values 41
(Lost Card), 43 (Stolen Card), 54 (Expired Card), etc. The following issuer
declines are not valid for an Account Status Inquiry Service transaction:
03 (Invalid Merchant), 12 (Invalid Transaction), 13 (Invalid Amount), 30

Account Status Inquiry Service transactions are not supported for Authorization
Advice/0120 and Reversal Request/0400 messages. If an acquirer sends an
Authorization Advice/0120—Acquirer-generated or Reversal Request/0400
message, the acquirer will receive an automated reply from the Authorization
Platform responding to the message where DE 39 contains value 12 (Invalid
transaction). DE 39, value 12 will be sent back in an automated reply for all
other message types.
*/

if pmtid='0100' or pmtid='0200' then

	-- changes by Anil 09 sept 2019 ( txn_checks )
	txn_checks_applied := txn_checks_applied | 16384::bigint; -- BIT 15 acc_enq_DE4 check
	
	-- changes by Anil 11 sept 2019 (txn_checks)
		txn_checks_applied := txn_checks_applied | 32768::bigint; -- BIT 16 acc_enq_POS check

      if  p4 > 0  then    ---- DECLINE account enquiry check if @p4 >0
       --  pp_info := pp_info | 512;
         p39o := '12';
         papprove_so_far := false;
         p38o := Null;
         p44o := '004';
         prsn_cde := '202';
         p48o := coalesce(p48o,'') || '8301' || 'U';
         pvalid_notes := coalesce(pvalid_notes,'')  || ': Acc Enq @p4<>0 set DE39= 30 ';
   		 txn_checks_results := txn_checks_results | 16384::bigint; -- BIT 15 acc_enq_DE4NotZero

     
      
      ELSEIF p3_1 <> '00'    AND p3_1 <> '28'  then  --SOH to check credit / status enq constraint.
        -- pp_info := pp_info | 1024;
         p39o := '30';
         papprove_so_far := false;
         p38o := Null;
         p44o := '003';
         prsn_cde := '203';
         p48o := coalesce(p48o,'') || '8301' || 'U';
         pvalid_notes := coalesce(pvalid_notes,'')  || ': Acc Enq @p3_1<>00 set DE39= 30 ';
      
      	 -- not POS
		 txn_checks_results := txn_checks_results | 32768::bigint; -- BIT 16 acc_enq_notPOS
        ELSE
        
         -- pp_info := pp_info | 2048;
          if papprove_so_far = true then
                 p39o := '85';
                 p38o := Null;
             prsn_cde := '204';
                --  set @p48o = @p48o + '8301' + 'U'
                 pvalid_notes := coalesce(pvalid_notes,'')  || ': Acc Enq no avs, no cvv req 00 ';
             else
                 p39o := '05';
                 p38o := Null;
             -- set @p48o = @p48o + '8301' + 'U'
                 pvalid_notes := coalesce(pvalid_notes,'')  || ': Acc Enq decline 05 ';
             End if;
      end if;
	  -- FIX TO ENSURE FOR INVALID CARDS WE RETURN RC = 95 for ASI cases
      -- changes by Anil 09 sept 2019 ( txn_checks )
	  txn_checks_applied := txn_checks_applied | 65536::bigint; -- BIT 17 acc_enq_invalidCard check
	
      if p2to = 9999 then
         p39o := '14';
         p38o := Null;
         prsn_cde := '112';
        --  set @p48o = @p48o + '8301' + 'U'
         pvalid_notes := coalesce(pvalid_notes,'')  || ': Acc Enq  - invalid card ';
      		 txn_checks_results := txn_checks_results | 65536::bigint; -- BIT 17 acc_enq_invalidCard
      end if;
   end if;
   
   
 end if;
 
END if;

 
 ---------------- ATM onnly allowed at Merchant categories 6010 or 6011 --------------   
   
   
  if  papprove_so_far = true then 
  
  if p3_1 = '01' AND (pMTID = '0100' or pMTID ='0200')then
     	-- changes by Anil 09 sept 2019 (txn_checks)
	txn_checks_applied := txn_checks_applied | 131072::bigint; -- BIT 18 atm_at_non_bank check

      if NOT (p18 = '6010' or p18 = '6011') then
         p39o := '03';
         p44 := '018';
         pvalid_notes := coalesce(pvalid_notes,'') || 'DECLINE: cash out in NON bank (MCC <> 6010/6011);';
         papprove_so_far := false;
         prsn_cde := '224';
         
         		 txn_checks_results := txn_checks_results | 131072::bigint; -- BIT 18 atm_at_non_bank
      end if;
   end if;  
  end if ;
   
------------------------------------------------------------------------------------

------------------ check if AVS check requested --------------------
IF papprove_so_far = TRUE THEN
    ---ROH 20201111--remvoing temp table and using array  pde48_array
    p48_82 = pde48_array[82];

    --select   se_val INTO p48_82 from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('82','') as INTEGER);
    IF p48_82 = '52' THEN
        -- AVS check requested inside account enquiry -check and put reply in @p48_83
        pdo_AVS_resp := TRUE;
        pvalid_notes := coalesce(pvalid_notes, '') || ': AVS check req 48_82=52 ';
    END IF;

    
    ---------------------- create AVS response in DE48_83 if required ----------------
    --set @pdo_AVS_resp = 0
    -- change by Anil (txn_checks)
    txn_checks_applied := txn_checks_applied | 2097152::bigint;
    -- BIT 22 avs processor check
    IF pdo_AVS_resp = TRUE THEN
        -- pp_info := pp_info || 4096;
        pAVS_Supported := TRUE;
        IF pAVS_Supported = TRUE THEN
            -- raise notice 'pcpo % pcad % ',pcpo,pcad;
            -- changes by Anil 09 sept 2019 (txn_checks)
            --ROH 2020-12-14 approve if avs match fails 
			---if Sets card to approve if AVS match fails =TRUE then approve de48_83='S'
            IF substring(ptok_flags,28,1) =1::bit then

                p48o := coalesce(p48o, '') || '8301' || 'S';
                pvalid_notes = pvalid_notes || ':AVS check . Sets card to approve if AVS match fails';

            ELSE
                ----AVS checking process ahead 
                IF pcad IS NULL THEN
                    pcad = '';
                END IF;
                txn_checks_applied := txn_checks_applied | 262144::bigint;
                -- BIT 19 avs processor data check
                
                IF pcpo = '' AND pcad = '' THEN
                    p48o := coalesce(p48o, '') || '8301' || 'U';
                    prsn_cde = '230';
                    pvalid_notes = pvalid_notes || ':AVS check . Processor has no AVS data';
                    txn_checks_results := txn_checks_results | 262144::bigint;
                    -- BIT 19 avs_no_processor_data
                ELSE
                    -- raise notice 'checking further AVS';
                    -- changes by Anil 09 sept 2019 (txn_checks)
                    txn_checks_applied := txn_checks_applied | 524288::bigint;
                    -- BIT 20 avs address check
                    txn_checks_applied := txn_checks_applied | 1048576::bigint;
                    -- BIT 21 avs postal code check
                    -- avs_complete_check_mismatch
                    txn_checks_applied := txn_checks_applied | 4194304::bigint;
                    -- BIT 23 avs_complete_check
                    IF p120_pc = pcpo AND p120_addr <> pcad AND pcad <> '' AND p120_addr <> '' THEN
                        p48o := coalesce(p48o, '') || '8301' || 'W';
                        pvalid_notes = pvalid_notes || ':AVS check - postcode matches, address does not';
                        prsn_cde = '231';
                        IF substring(ptok_flags, 28, 1) = 0::bit THEN
                            p39o = '05';
                            p38o = NULL;
                        END IF;
                        -- address mismatch
                        txn_checks_results := txn_checks_results | 524288::bigint;
                        -- BIT 20 avs_address_mismatch
                    END IF;
                    --For U.S. addresses, nine-digit postal code matches, address does not; for address outside the U.S., postal code matches, address does not.
                    IF p120_pc <> pcpo AND p120_addr = pcad THEN
                        p48o := coalesce(p48o, '') || '8301' || 'A';
                        pvalid_notes = pvalid_notes || ':AVS check  Address matches, postal code does not.';
                        prsn_cde = '232';
                            IF substring(ptok_flags, 28, 1) = 0::bit THEN
                                p39o = '05';
                                p38o = NULL;
                            END IF;
                            -- postal code mismatch
                            txn_checks_results := txn_checks_results | 1048576::bigint;
                            -- BIT 21 avs_postcode_mismatch
                    END IF;
                    --Address matches, postal code does not.
                    IF p120_pc = pcpo AND SUBSTR(p120_addr, 1, 5) = SUBSTR(pcad, 1, 5) THEN
                        p48o := coalesce(p48o, '') || '8301' || 'X';
                        pvalid_notes = pvalid_notes || ':AVS check  Postcode/zip and address match';
                        prsn_cde = '233';
                    END IF;
                    IF p120_pc <> pcpo AND p120_addr <> pcad THEN
                        RAISE notice 'pcpo % pcad % ', pcpo, pcad;
                        ---- SOH 26 May 2019 -- Hardcode to enable 'U' as AVS response for Limitz ------------  'U' means==> No data from issuer/Authorization Platform
                        ---Limitz product id's     114,112,116,120,118,122
                        IF (pprod_id IN (114, 112, 116, 120, 118, 122, 183)) THEN
                            p48o := coalesce(p48o, '') || '8301' || 'X';
                        ELSE
                            p48o := coalesce(p48o, '') || '8301' || 'N';
                        END IF;
                        --      p48o := coalesce(p48o,'') || '8301' || 'N';
                        prsn_cde = '234';
                        IF substring(ptok_flags, 28, 1) = 0::bit THEN
                            p39o = '05';
                            p38o = NULL;
                            pvalid_notes = pvalid_notes || ': Neither address nor postal code matches and flag set to decline this case: Decline(05)';
                            papprove_so_far = FALSE;
                        END IF;
                        -- postal code mismatch
                        txn_checks_results := txn_checks_results | 1048576::bigint;
                        -- BIT 21 avs_postcode_mismatch
                        -- address mismatch
                        txn_checks_results := txn_checks_results | 524288::bigint;
                        -- BIT 20 avs_address_mismatch
                        -- complete mismatch
                        txn_checks_results := txn_checks_results | 4194304::bigint;
                        -- BIT 23 avs_complete_check_mismatch
                        --  else
                        --  pvalid_notes = pvalid_notes  || ': Neither address nor postal code matches.' ;
                    END IF;
                        --N
                        --             if p48o not ilike '%8301%' then
                        --                p48o := coalesce(p48o,'') || '8301' || 'X';
                        --             end if;          --- 'U' removed temporarily
                        --             pvalid_notes := coalesce(pvalid_notes,'')  || ': AVS check rslt @p48o= ' || coalesce(p48o,'');
                
                END IF;    

            END IF;            
        ELSE --else of pAVS_Supported check 
            p48o := coalesce(p48o, '') || '8301' || 'Z';
            ---'S'
            pvalid_notes = pvalid_notes || ': AVS check not supported by Processor';
            prsn_cde = '237';
            txn_checks_results := txn_checks_results | 2097152::bigint;
             -- BIT 22 avs_no_processor_support
        END IF;
    END IF;
    --set @p48o = @p48o + '8301' + 'Z'
    --set @p39o='05'
    RAISE notice 'de48 %', p48o;
END IF;

------------- MSG TRACK ERROR TEST ----------------------------
if papprove_so_far is true then
--select * from @ptmpSubElements
	-- changes by Anil 06 sept 2019 (txn_checks)
	txn_checks_applied := txn_checks_applied | 32::bigint; -- BIT 6 mag_compliance check
     ---ROH 20201111--remvoing temp table and using array  pde48_array

   --if exists(select 1  from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('89','') as INTEGER)) then
     if pde48_array[89] is not null then
      p39o := '05';
      prsn_cde := '205';
      papprove_so_far := false;
            pvalid_notes := coalesce(pvalid_notes,'')  || 'MagStripe Compliance Fail (informed by acquirer) ' ;
	  -- error
	  txn_checks_results := txn_checks_results | 32::bigint; -- BIT 6 mag_compliance_error
    end if;
END if;

 
------------- Fleet Cards ----------------------------

  -- if exists(select 1  from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('98','') as INTEGER)) then
  --    p39o := '12';
  --    papprove_so_far := false;
  --     p44o := '002';
  --          pvalid_notes := coalesce(pvalid_notes,'')  || 'Fleet Card - decline (de48_98)' ;
  --  end if;

   --if exists(select 1  from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('99','') as INTEGER)) then
   --   p39o := '12';
   --   papprove_so_far := false;
   --   p44o := '003';
   --         pvalid_notes := coalesce(pvalid_notes,'')  || 'Fleet Card - decline (de48_99)' ;
   -- end if;

------------- INSTALLMENT Payments----------------------------

   ---ROH 20201111--remvoing temp table and using array  pde48_array

   --if exists(select 1  from tt_PTMPSUBELEMENTS where Field_id = 95 and se_val='MCINST') then
   if pde48_array[95]='MCINST' then 
   -- select   se_val INTO pDE048se38 from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('38','') as INTEGER);
            pvalid_notes := coalesce(pvalid_notes,'')  || 'Installment pmnt (48_95) ' ;
    end if;

----------------------------- CVC CVV 2 response in DE48 SE87 -----------------------

--Issuers must be able to receive CVC 2 requests in DE 48, subelement 92 and
--to respond with the appropriate value in DE 48, subelement 87.
--• The issuer, at its discretion, will send the acquirer an Authorization Request
--Response/0110 message where DE 39 may contain either the value 00
--(Approved), 05 (Do not honor), 85 (Not declined), or other valid business
--declines. Examples of other acceptable business declines include values 41
--(Lost Card), 43 (Stolen Card), 54 (Expired Card), etc. The following issuer
--declines are not valid for an Account Status Inquiry Service transaction:
--03 (Invalid Merchant), 12 (Invalid Transaction), 13 (Invalid Amount), 30

-- NOTE TO SHANE.... Account status enquiry - non keyed... check docs 14th Oct 16
 pvalid_notes := coalesce(pvalid_notes,'')  || 'Cvv Type:'  || pCT  || ' Rslt:' || pCV || '  ARQC:'  || pAC;
raise notice 'checking cvv and cvv type  %',papprove_so_far;

if  papprove_so_far = true then

--end if;
	-- changes by Anil 05 sept 2019 (txn_checks)
    if pCT = 1 then  --- SOH 17 Feb - reset @pcvto be =0

   		txn_checks_applied := txn_checks_applied | 8388608::bigint; -- BIT 24 cvv1 check

      if pCV = 0 and pAC <> 1 then  --- SOH 17 Feb - reset @pcvto be =0
      	-- mismatch
			txn_checks_results := txn_checks_results | 8388608::bigint; -- BIT 24 cvv1_mismatch
		-- pp_info := pp_info | 8192;
				
	      p48o := coalesce(p48o,'') || '8701' || 'Y';
	      p39o := '57';
	      prsn_cde := '216';
	      papprove_so_far := false;
	      p38o := NULL;
	      pvalid_notes := coalesce(pvalid_notes,'')  || ':CVV1 fail ';
   		end if;
   end if;
      
   -- changes by Anil 05 sept 2019 (txn_checks)
   if pCT = 2 then
      		txn_checks_applied := txn_checks_applied | 16777216::bigint; -- BIT 25 cvv2 check
	if pCV = 0 and pAC <> 1 then
			-- mismatch
			txn_checks_results := txn_checks_results | 16777216::bigint; -- BIT 25 cvv2_mismatch
		
             
             
             
             -- pp_info := pp_info | 16384;
              p48o := coalesce(p48o,'') || '8701' || 'N';
              if p61_7  = '8' then
                p39o := '85';
                prsn_cde := '218';
                 pvalid_notes := coalesce(pvalid_notes,'')  || ': Acc Status Enq ';
              else
			     p39o := '05';
                 papprove_so_far := false;
                 pvalid_notes := coalesce(pvalid_notes,'')  || ': CVV2 invalid ';
                 p38o := NULL;
                 prsn_cde := '217';
              end if;
			 pvalid_notes := coalesce(pvalid_notes,'')  || ': CVV2 fail ';
   end if;
   end if;

   -- changes by Anil 26 sept 2019 (txn_checks)
   if pCT = 3 then
    	txn_checks_applied := txn_checks_applied | 33554432::bigint; -- BIT 26 cvv3 check

    	if pCV = 0    and pAC <> 1 then
	    	-- mismatch 
			txn_checks_results := txn_checks_results | 33554432::bigint; -- BIT 26 cvv3_mismatch

			 p39o := '57';
	         papprove_so_far := false;
	         pvalid_notes := coalesce(pvalid_notes,'')  || ': CVV3 invalid ';
	         p38o := NULL;
	         prsn_cde := '600';

	    end if;
   end if;

   if pCT = 2 AND pCV = 1 then
                  if p61_7  = '8' then
                    p39o := '85';
                    prsn_cde := '218';
                     pvalid_notes := coalesce(pvalid_notes,'')  || ': Acc Status Enq ';
                   End if;

               -- pp_info := pp_info | 32768;
                  p48o := coalesce(p48o,'') || '8701' || 'M'; --Changed 
                  pvalid_notes := coalesce(pvalid_notes,'')  || ': CVV2 pass ';
   end if;
  
      if pCT = 0 AND pCV = 0 and pAC<> 1 then
              --  NO CVV provided, Not Chip... .is it Ecommerce?
              if p4 = 0 then
                                 p39o := '85';
                                 pvalid_notes := coalesce(pvalid_notes,'')  || ': NO CVV, NOT Chip, is 0 amt => De39=85 ';
                                 prsn_cde := '215';
              elseif p61_4 = '4' then   --  p22_1= '09' or  p22_1= '81'  or  p22_1= '82' then
                            -- is it recurring txn?  APprove allowed even if no CVV on recurring (add UCAF scrutiny later)
                            -- if p61_4 = '4' then
                                 p39o := '00';
                                 prsn_cde := '214';
                                 pvalid_notes := coalesce(pvalid_notes,'')  || ': NO CVV, NOT Chip, IS recurring => De39=00 ';
               					 pis_recurring=true;
		     elseif p61_7 = '4' then   --  pre-authorised transaction
					    raise notice 'pre- authorisation approving transaction ';
						 ---ROH 20201111--remvoing temp table and using array  pde48_array

						--select   trim(se_val) INTO _de48_63 from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('63','') as INTEGER);
						_de48_63=pde48_array[63];

						if _de48_63 is not null then 
							pvalid_notes =coalesce(pvalid_notes,'')  || ': pre-incremenatal authroisation ';
						end if;
						 p39o := '00';
						 prsn_cde := '214';
						 pvalid_notes := coalesce(pvalid_notes,'')  || ': NO CVV, NOT Chip, IS pre-authorised => De39=00 ';
							
                elseif p61_3 <> '1' then   --  NOTE NOTE NOTE ***** Customer specific logic can be added in here. -------
                                 p39o := '00';
                                 pvalid_notes := coalesce(pvalid_notes,'')  || ': NO CVV,NOT Chip,NOT off premesis=> De39=00 ';
                                 prsn_cde := '212';
                 Else
                                --removing mdes part from here 
								--ROH -2020-10-21 Approving MDES transaction 
								 if (pmtid='0100' or pmtid='0200') and pd48_26 is null then 
									
								 p39o := '05';
                                 papprove_so_far := false;
                                 prsn_cde := '213';
                                 pvalid_notes := coalesce(pvalid_notes,'')  || ': NO CVV, NOT Chip, Not Recurring , NO Mdes=> De39=05 ';
								 end if;
                End if;
  End if;
  
      -- (1) Addition by @abhishek
	  --2020-01-28 
	  -- AAV change part
	if aav <> 9 then
    	txn_checks_applied := txn_checks_applied | 144115188075855872::bigint; -- BIT 58 aav check
    	if aav<>1 then
		
		--service_3ds_status
-- 		0-3ds not enable
-- 		1-3ds enable 
-- 		2-3ds enable ,but client will enroll from there side
--		3-3ds enable, ignore aav attempt
		
        	if p3ds_status in (1,2) then
            	tempNote :=  ': Card enrolled to 3DS verification';
            	p39o:='05';
            	p38o:= NULL;
            	papprove_so_far := false;
                prsn_cde:='501';
            	txn_checks_results := txn_checks_results | 144115188075855872::bigint; -- BIT 58 aav_mismatch
        	elseif p3ds_status=10 then 
			  --3ds enable , and ignore the aav attempt case 
			  if aav<>7 then 
			  		--aav attempt case 
					--declining the transaction 
					p39o:='05';
					
			  end if;
			else 
            	tempNote := ': Card not enrolled to 3DS verification';
        	end if;

        	case aav 
            	when 0 then
                	tempNote:= tempNote || ' and 3ds check failed';
            	when 7 then
                	tempNote:= tempNote || ' and 3ds AAV Attempted'; 
            	when 8 then
                	tempNote:= tempNote || ' and key not found for AAV verification';
            	else
                	tempNote:= tempNote || ': but unsupported value received from paypower';
                end case;
    	else
        	tempNote := ': AAV verification success';
    	end if;
    	pvalid_notes:= coalesce(pvalid_notes,'') || tempNote;
	end if;
	-- (1)
End if;  --top end if

--------------------for MDES transaction ---------------------------
--ROH 2020-08-27
--https://support.banking.live/jira/browse/CS_SC-137
-----Getting if the transaction is MDES tranaction 
	
  if length(pd48_26)>2 then
	  txn_checks_applied := txn_checks_applied | (2^59)::bigint; -- BIT 59 MDES check 
	--  txn_checks_results := txn_checks_results | 288230376151711744::bigint; -- BIT 59 			
		--------wrong expiry case 
	  if p39o='14' then
		if POSITION('8701' IN p48o) = 0 then
		---8701 not added , adding it 
		  p48o := coalesce(p48o,'') || '8701' || 'N';
		 else 
		 ---replacing existing value 
		 p48o = REPLACE(p48o, '8701M', '8701N' );

		end if;
	  end if;
  pvalid_notes := coalesce(pvalid_notes,'')  || ': Mdes Transaction';

  end if;

---------------------------------------------------------------------

  if POSITION('8701' IN p48o) > 0 then
      pvalid_notes := coalesce(pvalid_notes,'')  || ': CVC check p48o= ' || coalesce(p48o,'');
   end if;
    
--------------------psd2 requirement ---------------------------------  
  raise notice 'Checking psd2';
  
  RAISE NOTICE 'aav %',aav;

if ppsd2_check=1 and	p22_1 in ('10','81') then  --if ecom then
    raise notice 'checking p2d parameter %',substring(pde48_array[22],1,2);
	if substring(pde48_array[22],1,2)='01' then
		pde48_22_1=substring(pde48_array[22],5,2); --Low-Risk Merchant Indicator
		raise notice 'Low-Risk Merchant Indicator 48_22_1 %',pde48_22_1;
	end if;
	
	if aav=1 then ---IF AAV /IAV then
		if p49 in ('978','975','191','203','208','348','985','946','752','352','756','578') then --If currency in (EEA currency eg : GBP, EUR,Corona etc ) 
		--Assume SCA is done
			raise notice 'Assume SCA is done';
			  pvalid_notes := coalesce(pvalid_notes,'')  || ': Assume SCA is done';

		else
			--outside of EEA ,proceed as normal
			raise notice 'outside of EEA ,proceed as normal';
     	  pvalid_notes := coalesce(pvalid_notes,'')  || ': outside of EEA ,proceed as normal';

		end if; --if currency check
	else  --if no aav/iav case then
		if p49 in ('978','975','191','203','208','348','985','946','752','352','756','578') then --If currency in (EEA currency eg : GBP, EUR,Corona etc ) 
			if pde48_22_1 in ('01','02','04') then -- if (low risk merchant indicator -DE48_22_1 in (01,02,04) then)//(MIT,TRA,Low Value)
				--SCA Exempted ,proceed
				raise notice 'SCA Exempted ,proceed'			;
				pvalid_notes := coalesce(pvalid_notes,'')  || ': SCA Exempted ,proceed';

				-- else if (low risk merchant indicator -DE48_22_1 in (03) then)//(recurring)
			elseif pde48_22_1 ='03' then
			raise notice 'checking low risk merchant indicator with recurring';
			if _de48_63 is null then 
				_de48_63=pde48_array[63];
			end if;
			raise notice 'incoming de48 %',_de48_63;

			 if exists (select 1 from txn_tok where DE2 =  p2to  and MTID = '0100' and strpos(_de48_63,TRIM(DE63_S1||DE63_S2||DE15)) >0) and p61_4='4' then
					raise notice 'recurring transaction';
			--	 if 	pis_pre_authorise is true or pis_recurring is true then 
					 -- if (trace id )
					-- sca exempted ,proceed 
					raise notice 'SCA Exempted ,proceed'			;
				     pvalid_notes := coalesce(pvalid_notes,'')  || ': SCA Exempted ,proceed';
				 else
				-- decline with de39=65
				raise notice 'decline with de39=65'			;
 		      p39o := '65';
				pvalid_notes := coalesce(pvalid_notes,'')  || ': SCA Exempted ,DECLINE';

	
				 end if;
			elseif pde48_22_1 in ('05','06') then  -- else if ((low risk merchant indicator -DE48_22_1 in (05,06) )  then --strong customer authentication SCA Delegation secure corporate payment 
			
				-- sca exempted ,proceed 
				raise notice 'SCA Exempted ,proceed'			;
				     	  pvalid_notes := coalesce(pvalid_notes,'')  || ': SCA Exempted ,proceed';

			else
			-- decline with de39=65
			raise notice 'decline with de39=65'			;
 		      p39o := '65';
				pvalid_notes := coalesce(pvalid_notes,'')  || ': SCA Exempted ,DECLINE';

			end if; --closing of elseif -low risk mercahant check

		end if;	--if currency check
	end if;---if aav-1 check 
end if;---if ecom check

--------------------psd2  ---------------------------------
-----------------------------------------------------------------------
---ROH 2020-11-12 --- Remove the velocity rule check from tok_pre 

--------------- SOH   27 Aug 2019 ---------- NO CVV BY COUNTRY EMERGENCY RULE ----------------------------
--Country list:
-- Argentina, Bolivia, Brazil, Chile, Colombia, Ecuador, Falkland Islands (Malvinas), French Guiana, Guyana, Paraguay, Peru, Suriname, Uruguay, Venezuela
-- 032, 068, 076, 152, 170, 218, 238, 254, 328, 600, 604, 740, 858, 862

--Afganistan, Bosnia, Ethiopia, Guyana, Iran, Iraq, Korea, Peoples Republic of Lao, Pakistan, Sri Lanka, Trinidad & Tobago, Tunisia, Uganda, Vanuatu, Yemen
--004,070,231,328,364,368,408,418,586,144,760,780,788,800,548,887

-- if  p61_13 in ('032', '068', '076', '152', '170', '218', '238', '254', '328', '600', '604', '740', '858', '862', '004','070','231','328','364','368','408','418','586','144','760','780','788','800','548','887') 
-- then
-- -- 		 p39o = '05';
-- -- 		 papprove_so_far=0;
-- -- 		 p38o = NULL;
-- -- 		 pvalid_notes = pvalid_notes  || ': No/Failed CVV in High Risk Country ' ;

-- --------------------------------  SOH   27 Aug 2019    Country Velocity constraint --------------------------------------

-- --raise notice 'p7 % p61_13% DE61_S13',to_timestamp(p7o, 'YYYYMMDDHH24MISS')::time,DATE_PART('hour', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::time - de7::time) * 60 + DATE_PART('minute', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::time- de7::time);
-- --;
-- --
-- --
-- -- select DATE_PART('hour', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::time - de7::time) * 60 + DATE_PART('minute', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::time- de7::time) 
-- -- into pcountry_vel_count from p_country_velocity;

-- select  count(*) into pcountry_vel_count from p_country_velocity  where de61_S13 = p61_13 and 
--  DATE_PART('hour', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::time - de7::time) * 60 + DATE_PART('minute', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::time- de7::time) <5
--  and mtid ='0100'and de61_s13 in ('032', '068', '076', '152', '170', '218', '238', '254', '328', '600', '604', '740', '858', '862', '004','070','231','328','364','368','408','418','586','144','760','780','788','800','548','887') ;

 
-- raise notice 'p7 %',pcountry_vel_count;
 
-- if pcountry_vel_count > 3 then

-- 		  p39o = '05';
-- 		 papprove_so_far=0;
-- 		 p38o = NULL;
-- 		 pvalid_notes = pvalid_notes  || ': Failed Veolcity check -  High Risk Country Rule ' ;
-- END if;

-- INSERT INTO p_country_velocity    (mtid ,de7 ,DE61_S13)
-- VALUES  (pMTID, to_timestamp(p7o, 'YYYYMMDDHH24MISS'), p61_13 ) ;

-- end if;  ---end of high risk country check 

-----------------------------------------------------------------------   
---------------- ON BEHALF ENHANCEMENT CHECKS --------------------
-----ROH -2020-10-21 ---Removing OBS CHeck 
--if papprove_so_far is true then
--   if exists(select 1  from tt_PTMPSUBELEMENTS where Field_id = '71') then 
--     select   se_val INTO pd48_71 from tt_PTMPSUBELEMENTS where Field_id = '71';
-- --       if prsn_cde in  ('212','213','214','215') then
	  
-- -- 		  if SUBSTR(pDE048se71,3,1) in ('V','C') then
-- -- 			  prsn_cde := '319';
-- -- 			  pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71 ... V or C';
-- -- 			  p39o := '00';
-- -- 			  if p4 = 0  and p3_1 <> '30' then
-- -- 			  	p39o := '85';
-- -- 			  end if;
			  
			  
			  
-- --     		end if;
			
-- --       	End if;
		
-- 		raise notice 'ROH : 2020-09-23 On Behalf testing';
		
-- 		 if pd48_71<>'' then

-- 		   --if obs value available checking the lenght and value 18
-- 		   if length(pd48_71)=4 and SUBSTR(pd48_71,1,2)='18' then
-- 			---Only OBS 18 is avaialble , 
-- 			--no overriding 
-- 			pvalid_notes := coalesce(pvalid_notes,'') || ' OBS ' ||pd48_71 || 'skipped';
			
-- 		   else 
-- 			   --Response code overriding with OBS check value
			 
-- 			  pd48_71ifid=4;
-- 			   raise notice 'pd48_71 %',pd48_71;

-- 			   while (pd48_71i < LENGTH(pd48_71)) LOOP

-- 			  pd48_71_sf1 = Substr(SUBSTR(pd48_71,pd48_71i,pd48_71ifid),1,2);
-- 			  pd48_71_sf2 = Substr(SUBSTR(pd48_71,pd48_71i,pd48_71ifid),3,1);

-- 			  raise notice 'pd48_71_sf1 % pd48_71_sf2 %',pd48_71_sf1,pd48_71_sf2;
-- 			  select issuer_decision,subfield2_description into  p39o,de48_71_description  from p_on_behalf_details where subfield_1=pd48_71_sf1  and subfield_2=pd48_71_sf2 ;

-- 			  raise notice '71SE_1 % 71SE_2 % issuer_decision % de48_71_description %',pd48_71_sf1,pd48_71_sf2,p39o,de48_71_description;
-- 			   if p39o is null then
-- 					 p39o='05';
-- 					 prsn_cde := '320';
-- 				   pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71 ' ||pd48_71_sf2 ||'  ' || 'Unknown subfield ';
-- 			  END if;

-- 			   if p39o='05' then
-- 				   prsn_cde := '320';
-- 				   pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71 ' ||pd48_71_sf2 ||'  ' || de48_71_description;

-- 				EXIT;
-- 			   END if;

-- 			   raise notice 'pd48_71i% value %',pd48_71i,SUBSTR(pd48_71,pd48_71i,pd48_71ifid) ;
-- 			   pd48_71i=pd48_71i+pd48_71ifid;

-- 			   END LOOP;
-- 		   end if; --end of obs 18 check only 
			
-- 		   end if; --end of se71 check

-- 		-- changes by Anil 10 sept 2019 (txn_checks)
--  --   	if SUBSTR(pDE048se71,1,2) = '02' then
-- --                 if  SUBSTR(pDE048se71,3,1) = 'T' then
-- --                         pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71: Valid ARQC/TC and ATC; TVR/CVR invalid';
-- --                         prsn_cde := '320';
-- --                          p39o := '05';
-- --                          papprove_so_far := false;
-- --                          p38o := NULL;
						 
-- -- 	  					txn_checks_applied := txn_checks_applied | 268435456::bigint; -- BIT 29 OB_4871_T check
-- -- 						txn_checks_results := txn_checks_results | 268435456::bigint; -- BIT 29 OB_4871_T
						 
-- --                 Elseif   SUBSTR(pDE048se71,3,1) = 'A' then
-- --                          pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71:Valid Cryptogram (AC); ATC outside allowed range';
-- --                          prsn_cde := '310';
-- --                          p39o := '05';
-- --                          p38o := NULL;
-- --                          papprove_so_far := false;
						 
-- -- 						 txn_checks_applied := txn_checks_applied | 536870912::bigint; -- BIT 30 OB_4871_A check
-- -- 						 txn_checks_results := txn_checks_results | 536870912::bigint; -- BIT 30 OB_4871_A
						
						 
-- --                 Elseif   SUBSTR(pDE048se71,3,1) = 'I' then
-- --                          pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71:Invalid Cryptogram';
-- --                          prsn_cde := '311';
-- --                          p39o := '05';
-- --                          papprove_so_far := false;
						 
-- -- 						 txn_checks_applied := txn_checks_applied | 1073741824::bigint; -- BIT 31 OB_4871_I check
-- -- 						 txn_checks_results := txn_checks_results | 1073741824::bigint; -- BIT 31 OB_4871_I
						 
-- --                 Elseif   SUBSTR(pDE048se71,3,1) = 'E' then
-- --                          pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71:Valid AC Crypto; ATC Replay';
-- --                          prsn_cde := '312';
-- --                          p39o := '05';
-- --                          papprove_so_far := false;
						 
-- -- 						 txn_checks_applied := txn_checks_applied | 2147483648::bigint; -- BIT 32 OB_4871_E check
-- -- 						 txn_checks_results := txn_checks_results | 2147483648::bigint; -- BIT 32 OB_4871_E
						 
-- --                 Elseif   SUBSTR(pDE048se71,3,1) = 'U' then
-- --                          pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71:Unable to process';
-- --                          prsn_cde := '313';
						 
-- -- 						 txn_checks_applied := txn_checks_applied | 4294967296::bigint; -- BIT 33 OB_4871_U check
-- -- 						 txn_checks_results := txn_checks_results | 4294967296::bigint; -- BIT 33 OB_4871_U
						 
-- --                 Elseif   SUBSTR(pDE048se71,3,1) = 'G' then
-- --                          pvalid_notes := coalesce(pvalid_notes,'') || 'ON BEHALF:DE48 SF71: AC Crypto valid but not an ARQC or TC, status of TVR/CVR unknown';
-- --                          prsn_cde := '314';
-- --                          p39o := '05';
-- --                          papprove_so_far := false;  
						 
-- -- 						 txn_checks_applied := txn_checks_applied | 8589934592::bigint; -- BIT 34 OB_4871_G check
-- -- 						 txn_checks_results := txn_checks_results | 8589934592::bigint; -- BIT 34 OB_4871_G
						 
-- --                 End if;
--    --     End if;
--      end if;

-- --end if;

  
------------------ END ONBEHALF ENHANCEMENT CHECKS ---------------------------------

 
 
 
-------------------------- SPLIT DE63 ---------------------------------
   if LENGTH(p63) > 0 then
      p63_1 := SUBSTR(p63,1,3);
      p63_2 := SUBSTR(p63,4,9);
   end if;

if papprove_so_far is true then

-- changes by Anil 10 sept 2019 (txn_checks)
	txn_checks_applied := txn_checks_applied | 17179869184::bigint; -- BIT 35 CshBk_at_POS_not check
	
if LENGTH(pDE048se61) >= 2 and SUBSTR(pDE048se61,2,1) = '1' and SUBSTR(p3,1,2) = '09' and  p39o = '00' then
      if  papprove_so_far = true then
         p39o := '87';
         pvalid_notes := coalesce(pvalid_notes,'')  || ': Cashback req at POS that does not support => @p39o=87 ';
         prsn_cde := '219';
      end if;
   		txn_checks_results := txn_checks_results | 17179869184::bigint; -- BIT 35 CshBk_at_POS_not_supported
   end if;
 /* 5. FALLBACK FAIL... Fallback to magnetic stripe on hybrid cards ==> if @p22_1= 80 ==> @p39o= 05 , @pnote='De22=80 fallback => Decline'*/
   if LENGTH(p22) > 1 and SUBSTR(p22,1,2) = '80' and  p39o = '00' then
      p39o := '05';
      prsn_cde := '220';
      papprove_so_far := false;
      pnote := 'De22=80 fallback => Decline';
      pvalid_notes := coalesce(pvalid_notes,'')  ||  ': De22=80 fallback => DE39=05';
   end if;
/* 7. FALLBACK FAIL  fb to PAN manual entry on hybrid cards: if @p22_1 = 79 ==> @p39o= 05 , @pnote='De22=79 fallback => Decline'*/
   if LENGTH(p22) > 1 and SUBSTR(p22,1,2) = '79' and  p39o = '00' then
      p39o := '05';
      prsn_cde := '221';
      papprove_so_far := false;
      pvalid_notes := coalesce(pvalid_notes,'')  || ': De22=79 fallback =>  DE39=05 ';
   end if;

end if;

  
   if p48o = '' then
      p48o := p48;
   end if;

----------------------- IS EXTERNAL AUTH ? ------------------------------------------------

--             if substring(ptok_flags,32,1) =1::bit then
--     		  pauth_type :=0;
--             else 
--             pauth_type :=1;
--             end if;
-----------------------------------------------------------------
         --   pauth_type :=2;

-- ------------------------ DE38 handling---------------------------------------------------

if (pmtid = '0100' or pmtid = '0200' ) and p39o in ('10','00','87','85') or (substring(p124,1,2) in ('AC','TC','TV')) or pcn='P' then
        if (p38i > 999000 or p38i < 100000) then p38i = 100100; end if;
        p38i = p38i +  cast ( random()*20 as int ) + 1;
        p38o = p38i;
		p38o_prosa=p38o;
        --pupdate_tok = pupdate_tok || ', DE38 = ' || p38i::varchar ;   -- means   update p_tok set DE38 = p38i where pr_tok = p2to ;
		--ROH : 2020-11-09 
		--Removing dynamic p_tok update and assignning value for final update 
		p38_final=p38i;
   ---ROH 20201111--remvoing temp table and using array  pde48_array
	  --if exists(select 1  from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('38','') as INTEGER)) then
   if pde48_array[38] is not null then 
--      select   se_val into pDE048se38  from tt_PTMPSUBELEMENTS where Field_id = '38';
      pDE048se38=pde48_array[38];

      p38o =  LEFT(p38o,5) ||  pDE048se38 ;--  overlay(@38o placing @DE048se38 from 6 for 1)
	  p38o = REPLACE(p38o, ' ', '0' );   --REPLACE(STR(p38o, 6), SPACE(1), '0');
	 
      pvalid_notes = pvalid_notes || 'De48_SE38 => de38=   '  ||  p38o ;
      p48o = Replace(p48,'3801' || pDE048se38,'');
    end if;

End if;
raise notice 'DE 38 Generated % ',p38o;

------------------------------------------------------------------

      --  if @serv_code <> SUBSTRing(@35,7,3)  AND SUBSTRing(@35,7,3) <> '101' AND SUBSTRing(@35,7,3) <> ''

-------------- service code check -- anti fraud check -----------
if papprove_so_far is true then
  if p35<>'0' then
	-- changes by Anil 09 sept 2019 ( txn_checks)
		txn_checks_applied := txn_checks_applied | 68719476736::bigint; -- BIT 37 service_code_mismatch check
		
        if pserv_code <> SUBSTR(p35,7,3)AND SUBSTR(p35,7,3) <> '101' AND SUBSTR(p35,7,3) <> '' then
               prsn_cde := '290';
               p39o := '05';
               p38o := NULL;
               pvalid_notes := coalesce(pvalid_notes,'') || ': service code mismatch DE35 Srv code: ' || SUBSTR(p35,7,3) ;
       		   txn_checks_results := txn_checks_results | 68719476736::bigint; -- BIT 37 service_code_mismatch

            end if;
         end if;
end if;
--------------end of  service code check -- anti fraud check -----------

--------------------------  pstat_nwk   (Network Status) OVERRIDES ------------------------
--------------------------  @stat_nwk   (Network Status) OVERRIDES ------------------------
--1000  Fully Operational              DE39=00
--1001  Refer to Issuer                DE39= 01
--1004  Capture Card                   DE39=04
--1005  Decline All Txns               DE39=05
--1008  Honour with ID                 DE39=08
--1041  Lost Card - Capture           DE39= 41
--1043  Stolen Card - Capture         DE39=43
--1054  Expired card - Report         DE39=54
--1140  Lost Card - NO Capture       DE39= 05
--1143  Stolen card - NO Capture     DE39=05
--1154  Expired Card - Dont Inform   DE39=05
--1199  Void Card                    DE39=05
if papprove_so_far is true then
	if pstat_nwk <> 1000  and (pmtid = '0100' or pmtid = '0200') then
	
	-- changes by Anil 10 sept 2019 (txn_checks)
	
    if pstat_nwk = 1005 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override :to force decline';
        prsn_cde := '400';
        p39o := '05';
	-- added
			txn_checks_applied := txn_checks_applied | 549755813888::bigint; -- BIT 40 NSO_1005 check
			txn_checks_results := txn_checks_results | 549755813888::bigint; -- BIT 40 NSO_1005
                  
    elseif pstat_nwk = 1001 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Refer card to Issuer';
        prsn_cde := '401';
        p39o := '01';
	-- added
			txn_checks_applied := txn_checks_applied | 137438953472::bigint; -- BIT 38 NSO_1001 check
			txn_checks_results := txn_checks_results | 137438953472::bigint; -- BIT 38 NSO_1001
		
        elseif pstat_nwk = 1004 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Capture card (beware of cost)';
        prsn_cde := '402';
        p39o := '04';
	-- added
			txn_checks_applied := txn_checks_applied | 274877906944::bigint; -- BIT 39 NSO_1004 check
			txn_checks_results := txn_checks_results | 274877906944::bigint; -- BIT 39 NSO_1004
		
        elseif pstat_nwk = 1008 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Honour Txn with ID';
        prsn_cde := '404';
        p39o := '08';
	-- added
			txn_checks_applied := txn_checks_applied | 1099511627776::bigint; -- BIT 41 NSO_1008 check
			txn_checks_results := txn_checks_results | 1099511627776::bigint; -- BIT 41 NSO_1008
		
       
        elseif  pstat_nwk = 1041 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Lost card (Capture + costs)';
        prsn_cde := '405';
        p39o := '41';
        	-- added
			txn_checks_applied := txn_checks_applied | 2199023255552::bigint; -- BIT 42 NSO_1041 check
			txn_checks_results := txn_checks_results | 2199023255552::bigint; -- BIT 42 NSO_1041
			
        elseif pstat_nwk = 1043 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Stolen card (Capture + costs)';
        prsn_cde := '406';
        p39o := '43';
	-- added
			txn_checks_applied := txn_checks_applied | 4398046511104::bigint; -- BIT 43 NSO_1043 check
			txn_checks_results := txn_checks_results | 4398046511104::bigint; -- BIT 43 NSO_1043
		
        elseif pstat_nwk = 1054 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Expired Card';
        prsn_cde := '407';
        p39o := '54';
        	-- added
			txn_checks_applied := txn_checks_applied | 8796093022208::bigint; -- BIT 44 NSO_1054 check
			txn_checks_results := txn_checks_results | 8796093022208::bigint; -- BIT 44 NSO_1054
			
		elseif pstat_nwk = 1062 then
			pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Restricted Card - general decline';
			prsn_cde := '410';
			p39o := '62';
			-- added
			txn_checks_applied := txn_checks_applied | (2^59)::bigint; -- BIT 60 NSO_1062 check
			txn_checks_results := txn_checks_results | 576460752303423488::bigint; -- BIT 60 NSO_1062
			
		
        elseif pstat_nwk = 1140 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Lost card - general decline (no cost)';
        prsn_cde := '408';
        p39o := '05';
	-- added
			txn_checks_applied := txn_checks_applied | 17592186044416::bigint; -- BIT 45 NSO_1141 check
			txn_checks_results := txn_checks_results | 17592186044416::bigint; -- BIT 45 NSO_1141
		
        elseif pstat_nwk = 1143 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Stolen card general decline (no cost)';
        prsn_cde := '409';
        p39o := '05';
	-- added
			txn_checks_applied := txn_checks_applied | 35184372088832::bigint; -- BIT 46 NSO_1143 check
			txn_checks_results := txn_checks_results | 35184372088832::bigint; -- BIT 46 NSO_1143
		
        elseif pstat_nwk = 1154 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : Expired card - general decline';
        prsn_cde := '410';
        p39o := '05';
	-- added
			txn_checks_applied := txn_checks_applied | 70368744177664::bigint; -- BIT 47 NSO_1154 check
			txn_checks_results := txn_checks_results | 70368744177664::bigint; -- BIT 47 NSO_1154
	
        elseif pstat_nwk = 1199 then
      pvalid_notes := coalesce(pvalid_notes,'') || 'Nwk_status override : void card general decline (no cost)';
        prsn_cde := '411';
        p39o := '05';
       	-- added
			txn_checks_applied := txn_checks_applied | 140737488355328::bigint; -- BIT 48 NSO_1199 check
			txn_checks_results := txn_checks_results | 140737488355328::bigint; -- BIT 48 NSO_1199
		
        end if;
	end if;	
end if;
--------------------------end of  pstat_nwk   (Network Status) OVERRIDES ------------------------

--for pre_authorised transcation

--yes de61_7=4 --Pre-authorised .
--if de48_63 present then it is pre-authorised incremental 
--else it is pre-authroised 
 
 -------------------------------  CREATE UNIQUE ID .. TXN_ID  -----------------------------------------  
if p11 is null then 
  p11 := '000000';
end if;
if p32 is null then 
  p32 := '000000';
end if;

   --if pMTID ilike '04%' then
     if length (p90) >0 then
	
			pmti_orig ='0100';
			p11_orig ='000000';
			p7_orig ='0101000001';
			p32_orig ='100000';
			p7_origR ='0101000001';
			pmti_orig := SUBSTR(p90,1,4);
			p11_orig := SUBSTR(p90,5,6);
			p7_orig := SUBSTR(p90,11,10);
			
			p32_orig := SUBSTR(SUBSTR(p90,21,11),length(SUBSTR(p90,21,11)) -6+1);
					
		  if date_part('month', now())  = CAST(left(p7,2) AS SMALLINT)  then
			  ptxn_yr_loc = date_part('year', now());
		  elseif date_part('month', now())  = 1 and left(p7,2) = '12' then
				ptxn_yr_loc = date_part('year', now())-1;
		  elseif date_part('month', now())  = 12 and left(p7,2) = '01' then
			ptxn_yr_loc = date_part('year', now())+1;
		  end if;
				
				p7_orig := ptxn_yr_loc || p7_orig;
				p7_origR = p7_orig;
				raise notice 'p7_orig % p11_orig %',p7_orig,p11_orig;
				p7_orig=to_timestamp(p7_orig, 'YYYYMMDDHH24MISS')::varchar;
	
--         pti :=    overlay('0000000' placing cast (DATE_PART('day', to_timestamp(ptxn_yr_loc || p7_orig, 'YYYYMMDDHH24MISS') - '01 Jan 2017 00:00:00'::timestamp) as character varying)||
-- 						  cast (DATE_PART('second', to_timestamp(ptxn_yr_loc || p7_orig, 'YYYYMMDDHH24MISS')) as character varying)from 1) || coalesce(p32_orig,'') || coalesce(p11_orig,''); 
   
  -- pti = DATE_PART('day', p7_orig::timestamp - '01 Jan 2017 00:00:00'::timestamp)::varchar ||  DATE_PART('second',p7_orig::timestamp)::varchar   ||  p32_orig || p11_orig;
--   pti :=   overlay('0000000' placing cast (DATE_PART('day', p7_orig::timestamp - '01 Jan 2018 00:00:00'::timestamp) as character varying)||  
-- cast (DATE_PART('second', p7_orig::timestamp) as character varying) from 1) || right(p32_orig,6) || p11_orig;
 --Commented on 2021-01-13
 --pti :=   DATE_PART('day', p7_orig::timestamp - '01 Jan 2017 00:00:00'::timestamp) || overlay('00' placing cast (dATE_PART('second', p7_orig::timestamp) as character varying) from 1) || right(p32_orig,6) || p11_orig;
--ROH : 2021-01-13 changed the txn-id algorithm
-- 4 of date 
-- 5 of seconds
-- 6 of STAN  - DE11
-- 4 DE32
  pti :=  DATE_PART('day', p7_orig::timestamp - '01 Jan 2017 00:00:00'::timestamp) ||
 		lpad(extract(epoch FROM p7_orig::time)::double precision::character varying,5,'0')||
		right(p32_orig,4) || p11_orig;	
	
  --ROH july 15,2020 pre-authorisied transaction
  ---ROH 20201111--remvoing temp table and using array  pde48_array
 --   elseif  exists (select 1  from tt_PTMPSUBELEMENTS where Field_id='63') AND pMTID like '01%'  AND  p61_7='4' then
  elseif  pde48_array[63] is not null AND pMTID like '01%'  AND  p61_7='4' then
   
   
 --raise notice 'pre-incremenat authorisation';
  -- select   trim(se_val) INTO _de48_63 from tt_PTMPSUBELEMENTS where Field_id = cast(NULLIF('63','') as INTEGER);
 	if _de48_63 is  null then 						
		_de48_63=pde48_array[63];

	end if;
 	select coalesce(txn_id,'0') into pti1 from txn_tok where DE2 =  p2to  and MTID like '01%' and strpos(_de48_63,TRIM(DE63_S1||DE63_S2||DE15)) >0 ;--order by tt_id desc limit 1;  --if @48o like '%8901G%'
	pti = coalesce (pti1,'0');
    --for incrementent transaction for mambu , paypower need to decide wheather to call fast hold/unhold api so 
	--pre-tok supplying further instruction to call the paycore db
	pinstr = pinstr  ||   '10;INCRE;';
	pis_pre_authorise=true;
	raise notice 'original txn_id % for _de48_63 %',pti1,_de48_63;
   ---ROH 20201111--remvoing temp table and using array  pde48_array
   --elseif  exists (select 1  from tt_PTMPSUBELEMENTS where Field_id='63') AND pMTID like '01%'  AND  p61_7='5' then
	pvalid_notes = pvalid_notes  ||  ': pre-authorised request';

   elseif  pde48_array[63] is not null AND pMTID like '01%'  AND  p61_7='5' then
 ---------------Incremental Auth checks ---------------------------------
	 if _de48_63 is  null then 						
		_de48_63=pde48_array[63];

	end if;
 
 raise notice 'txn_id generated';

	select coalesce(txn_id,'0') into pti1 from txn_tok where DE2 =  p2to  and MTID like '01%' and  strpos(p48,TRIM(DE63_S1||DE63_S2)) >0
	order by tt_id desc limit 1;  --if @48o like '%8901G%'
	pti = coalesce (pti1,'0');
		pis_pre_authorise=true;
	
	 --for incrementent transaction for mambu , paypower need to decide wheather to call fast hold/unhold api so 
	--pre-tok supplying further instruction to call the paycore db
	pinstr = pinstr  ||   '10;INCRE;';
	---------------------------------- ----------------------------
-- 	---------------Incremental Auth checks ---------------------------------
--     declare @ti1 varchar(20) = NULL
-- 	select top 1 @ti1=ISNULL(txn_id,@ti)  from Txn_Tok (NOLOCK) where DE2 =  @2to  and MTID like '01%' and  CHARINDEX (RTRIM(DE63_S1+DE63_S2), @48) >0   order by tt_id desc  --if @48o like '%8901G%'
-- 	set @ti = ISNULL (@ti1,@ti)
-- 	---------------------------------- ----------------------------
 
--   select De2,txn_id into p2to,pti from Txn_Tok where DE7 =  to_timestamp(p7o, 'YYYYMMDDHH24MISS') and DE11 = p11 order by tt_id desc  limit 1;
--   select   pb_tok, p_card_product.client_id INTO ppb_tok, pclient_id from p_tok  inner join p_card_product on  p_tok.crd_prdct_id = p_card_product.crd_prdct_id where pr_tok = p2to limit 1;
   ---------------------------------- ----------------------------
    
    else

    --pti :=   overlay('0000000' placing cast (DATE_PART('day', to_timestamp(p7o, 'YYYYMMDDHH24MISS') - '01 Jan 2017 00:00:00'::timestamp) as character varying)||  cast (DATE_PART('second', to_timestamp(p7o, 'YYYYMMDDHH24MISS')) as character varying) from 1) || p32 || p11; 
-- pti :=   overlay('0000000' placing cast (DATE_PART('day', to_timestamp(p7o, 'YYYYMMDDHH24MISS') - '01 Jan 2017 00:00:00'::timestamp) as character varying)||  
--    pti  = cast (DATE_PART('day', to_timestamp(p7o, 'YYYYMMDDHH24MISS')-'01 Jan 2017 00:00:00') || right(p32,6) || p11;
--     raise notice 'generation txn_id p7_original % p7o % , p32 %, p11 % % ',p7_orig ,p7o ,p32,p11,	DATE_PART('day', p7o - '01 Jan 2017 00:00:00');
-- 	--pti = convert(varchar(15), DATEDIFF (d,'01 Jan 2017 00:00:00',@7o)) + Convert(varchar(10), DATEPART(SS, @7o))  +  @32+ @11
-- 	pti= DATE_PART('day', p7_orig::timestamp, '01 Jan 2017 00:00:00'::timestamp) ||  DATE_PART('second',p7o)   ||  p32 || p11;
	--pti = convert(varchar(15), DATEDIFF (d,'01 Jan 2017 00:00:00',p7o)) || Convert(varchar(10), DATEPART(SS, p7o))  ||  p32 || p11;
	raise notice 'de32 %, de11 %, de 7 %',p32,p11,p7o;
	
-- 	pti :=   overlay('0000000' placing cast (DATE_PART('day', to_timestamp(p7o, 'YYYYMMDDHH24MISS') - '01 Jan 2018 00:00:00'::timestamp) as character varying)||  
-- cast (DATE_PART('second', to_timestamp(p7o, 'YYYYMMDDHH24MISS')) as character varying) from 1) || right(p32,6) || p11;

--pti :=   DATE_PART('day', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::timestamp - '01 Jan 2017 00:00:00'::timestamp)	||  overlay('00' placing cast (dATE_PART('second', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::timestamp) as character varying) from 1) || right('000000' || p32,6) || p11;	
 
--ROH : 2021-01-13 changed the txn-id algorithm
-- 4 of date 
-- 5 of seconds
-- 6 of STAN  - DE11
-- 4 DE32
 pti :=  DATE_PART('day', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::timestamp - '01 Jan 2017 00:00:00'::timestamp) ||
 		lpad(extract(epoch FROM to_timestamp(p7o, 'YYYYMMDDHH24MISS')::timestamp::time)::double precision::character varying,5,'0')||
		right(p32,4) || p11;
    end if;

if pti is null or pti='0' then
--pti :=   DATE_PART('day', to_timestamp(p7o, 'YYYYMMDDHH24MISS') - '01 Jan 2017 00:00:00'::timestamp)	||  overlay('00' placing cast (dATE_PART('second', to_timestamp(p7o, 'YYYYMMDDHH24MISS')) as character varying) from 1) || right('000000' || p32,6) || p11;	
pti := DATE_PART('day', to_timestamp(p7o, 'YYYYMMDDHH24MISS')::timestamp - '01 Jan 2017 00:00:00'::timestamp) ||
 		lpad(extract(epoch FROM to_timestamp(p7o, 'YYYYMMDDHH24MISS')::timestamp::time)::double precision::character varying,5,'0')||
		right(p32,4) || p11;
end if;
ptxn_id := pti; 
pti = coalesce(pti, '0');
if ptxn_id = '' then
	ptxn_id = pti ;
end if;	
-----------------------------------------------------------------
------------------------ Reason codes for advisements cases ---------------------------------
--For Fuel transaction 
if pmtid ='0120' and p18='5542' then

	--for AFD transaction for mambu , paypower need to decide wheather to call fast hold/unhold api so 
	--pre-tok supplying further instruction to call the paycore db
	pinstr = pinstr  ||   '10;INCRE;';

end if;

if pmtid ilike '%020' then
--prsn_cde := p60_1;
p39o := '00';
p38o := Null;
p48o = Null;
end if;
----------------------------------

------------------------------------ CRYPTOGRAM CHECKS ---------------------------------
-- changes by Anil 09 sept 2019 ( txn_checks )and condition add by @abhi on 16th Aug 2020 
if pAC <> 9 and papprove_so_far = true then
	txn_checks_applied := txn_checks_applied | 281474976710656::bigint; -- BIT 49 ac_crypto check
end if;

raise notice 'checking the approval: %',papprove_so_far;

if pAC = 1 and papprove_so_far=true then
	if pat_in-patc <-5 or pat_in-patc > 5 then -- ATC Out of range	
		 p39o= p39o;
	END if;

 ------------------------- NEW PIN MANAGEMENT ------------------
 ---------pin change-------------------------------------------    
	raise notice 'New Pin Management';
    -- pin change requested internally (NOT via 0100 (ATM etc))
    -- select * from m_tok_description
			 
			  if substring(ptok_flags,2,1) =1::bit then -- & 1073741824 =1073741824
				BEGIN
			  		raise notice ' Pin change from tok_flag';
					IF pinstr ='00' then 
						pinstr ='' ;
					END if;
					pinstr = pinstr  ||   '01;'	  ||  encode(ppvv,'hex')  || ';'; -- SUBSTRing(CAST(@pvv AS VARCHAR(36)),3,36) + ';'
					--set @update_tok = @update_tok   +   ', tok_flags = tok_flags & B''10111111111111111111111111111111''';  -- means  update p_tok set tok_flags = tok_flags & B'10111111111111111111111111111111' where pr_tok = p2to ;
					--pupdate_tok = pupdate_tok   ||   ', tok_flags = tok_flags & B''10101111111111111111111111111111''';  
					
					--ROH : 2020-11-09
					--Removing dynamic query for p_tok update 
					ptok_flags_final=ptok_flags_final & B'10111111111111111111111111111111';
					
					pvalid_notes = pvalid_notes  ||  ': Pin change script sent ';
					INSERT INTO p_pvv_history   (pr_tok ,pvv,key_id ,cre_dt, note)
								VALUES  (p2to, ppvv, pkey_id, now(), 'Pin change requested internally. Card Flags before change: ');
					pvalid_notes = pvalid_notes  ||  ': Pin change sent to Chip ';
				END;	
 			 ---------------------- END of IF part for the tok_flag check for pin management --------------------
			 ELSE
			 raise notice ' Pin change further';

			  if length(ppvv_new) >1 AND p3_1 = '92'  and pac = 1 then
				if papprove_so_far = true then
				  BEGIN
						 if p4 = 0 then							  
							p39o = '00' ; -- '85'
							prsn_cde = '264';  --DE39 set to 85 because it is approve but no amount included (de4=0)
						 else
							p39o = '00' ;
						 end if;
				   prsn_cde = '223';--	Pin change requested via (ATM)
				   IF pinstr ='00' then pinstr ='';end if;
				   pinstr = pinstr  ||   '01;YES;';
					raise notice 'new pvv %',encode(ppvv_new,'hex');
					
					--pupdate_tok = pupdate_tok   ||   ', tok_flags = tok_flags & B''10101111111111111111111111111111'', pvv= ''\x'||encode(ppvv_new,'hex')||'''';--|| encode(ppvv_new,'hex');
					
					--ROH : 2020-11-09
					--Removing dynamic p_tok update and assigning to the variable for update
					 --tok_flags = tok_flags & B''10101111111111111111111111111111'', pvv= ''\x'||encode(ppvv_new,'hex')||'''';--|| encode(ppvv_new,'hex');
					ptok_flags_final=ptok_flags_final & B'10101111111111111111111111111111';
					ppvv_final =ppvv_new;
					ppvv_hsm_final =ppvv_new;
					
					INSERT INTO p_pvv_history   (pr_tok ,pvv,key_id ,cre_dt, note)
					VALUES  (p2to, ppvv, pkey_id, now(), 'Pin change requested in auth, set in system and being sent to chip');
						pvalid_notes = pvalid_notes  ||  ': Pin change sent to Chip ';

				END;
				else
	               pvalid_notes = pvalid_notes   ||  ': Cannot issue pin change script as txn is a decline ';
				end if; --end of if approve as part 
				
			  end if;----end of else part of pvv_new part  check 	
			 END if; ---End of tok_flag check
			 
		-- SOH added check: if CVR byte 3 indicates offline remaining pin tries  = 0 AND current pin in De52 is good then 
		--allow unblock script to go to chip. 
		--		raise notice 'p55_9f10 %  pin counter %',encode(p55_9F10,'hex'),substring(encode(p55_9F10,'hex'),9,2)::int;
		if 	lower(pcn) <> lower('JTCO') then
		--ROH : 2020-09-30 :
		--Pin counter taking from  substring(encode(p55_9F10,'hex'),9,2)===> substring(encode(p55_9F10,'hex'),10,1)
		pchip_pin_tries = substring(encode(p55_9F10,'hex'),10,1)::int;--substring(p55_9F10,5,1);-- & 15
		---ROH : 2021-06-28 setting up the pin chip tries value from the incoming 9f10
		ppin_rem_chp=pchip_pin_tries;
		END if;
		-- SOH 27-Nov-2019 change =0 to be <3 below
		if pchip_pin_tries < 1  and ppvv = p52 then
		-- SOH   27-Nov-2019 change '01000000000000000000000000000000' to '00010000000000000000000000000000' below
		ptok_flags = ptok_flags | B'00010000000000000000000000000000'  ;  ---updating tok flage to send pin unblock
		end if;
		
	-- changes by Anil  06 Sep 2019   (txn_checks)
	--if length(p55_9F10) > 2 then --  more than 2, 3rd byte now required

		cvr_first_byte = get_byte(p55_9F10, 2) ;--CAST (substring(encode(p55_9F10 ,'hex'),3,1) AS smallint) ;
		--raise notice 'p55 %', 'FIX1';
	
		-- if  (B'00000100' = (B'00000100' & cvr_first_byte))  or (B'00000010' = (B'00000010' & cvr_first_byte))  then
		IF p22_1 not in ('07', '91') THEN
	       IF (4 = (4 & cvr_first_byte)) OR (2 = (2 & cvr_first_byte)) THEN

	           txn_checks_applied := txn_checks_applied | 1::bigint;
	           -- BIT 1 offline_pin checked
	           
	           IF (0 = (1 & cvr_first_byte)) THEN
	               txn_checks_results := txn_checks_results | 1::bigint;
	               -- BIT 1 offline_pin_mismatch
	           END IF;
	       END IF;
       END IF;

	--end if;

	-- changed by Anil 26 sept 2019 (txn_checks)
	--if length(p55_9f10) > 5 then

		cvr_fourth_byte = get_byte(p55_9F10, 5); --CAST (substring(encode(p55_9F10 ,'hex'),6,1) AS smallint) ;
		txn_checks_applied := txn_checks_applied | (2^50)::bigint; -- BIT 51 offline_pin

		-- if  ( B'00100000' = (B'00100000' & cvr_fourth_byte))  then
		if  ( 32 = ( 32 & cvr_fourth_byte))  then
			txn_checks_results := txn_checks_results | (2^50)::bigint; -- BIT 51 offline_pin_not_checked
		end if;

		txn_checks_applied := txn_checks_applied | 1024::bigint; -- BIT 11 pin_tries_ofl check
		
		-- if  ( B'00001000' = (B'00001000' & cvr_fourth_byte))  then
		if  ( 8 = ( 8 & cvr_fourth_byte))  then
			txn_checks_applied := txn_checks_applied | 1024::bigint; -- BIT 11 pin_tries_ofl_exceeded
		end if;
	--end if;

	--if length(p55_9F10) > 6 then
		-- changes by Anil 09 sept 2019 ( txn_checks )
							
	--010103a00300dac1
		cvr_fifth_byte =  get_byte(p55_9F10, 6); --CAST (substring(encode(p55_9F10 ,'hex'),7,1) AS smallint) ; -- substring(p55_9F10,7,1);-- substring(p55_9F10,7,1);
		txn_checks_applied := txn_checks_applied | 18014398509481984::bigint; -- BIT 55 chip_script check
		txn_checks_applied := txn_checks_applied | 9007199254740992::bigint; -- BIT 54 chip_received check
		txn_checks_applied := txn_checks_applied | 4503599627370496::bigint; -- BIT 53 Chip_ issuer_auth check
		
		--raise notice 'Debugging here %',cvr_fifth_byte;

		-- if(B'00000100' = (B'00000100' & cvr_fifth_byte) )then 
		if(4 = (4 & cvr_fifth_byte) )then 
			txn_checks_results := txn_checks_results | 4503599627370496::bigint; -- BIT 53 Chip_ issuer_auth_failed
		end if;

-- if( B'00000001' = (B'00000001' & cvr_fifth_byte)) then
		if( 1 = (1 & cvr_fifth_byte)) then
			txn_checks_results := txn_checks_results | 18014398509481984::bigint; -- BIT 55 chip_script_fail
		end if;

-- if( B'00000010' = (B'00000010' & cvr_fifth_byte)) then
		if( 2 = (2 & cvr_fifth_byte)) then
			txn_checks_results := txn_checks_results | 9007199254740992::bigint; -- BIT 54 chip_received_script
		end if;

		-- changes by Anil 11 sept 2019 (txn_checks)
		txn_checks_applied := txn_checks_applied | 36028797018963968::bigint; -- BIT 56 chip_pos_unable_to_go_online  check

-- if( B'01000000' =  ( B'01000000' & cvr_fifth_byte) ) then
		if( 64 =  ( 64 & cvr_fifth_byte) ) then -- substring(p55_9F10,6,1)) CVR byte 4
			txn_checks_results := txn_checks_results | 36028797018963968::bigint; -- BIT 56 chip_pos_unable_to_go_online
		end if;
	--end if;
	
		---ROH 2020-11-09 
		--Removing dynamic update of p_tok and assignning value to the variable
		
		if pchip_pin_tries = 0  then
		    --pupdate_tok = pupdate_tok ||  ', pin_try_rem_chp=0 ';
			ppin_rem_chip_final=0;
		elseif pchip_pin_tries = 1 then 
			--pupdate_tok = pupdate_tok  ||  ', pin_try_rem_chp=1 ';
			ppin_rem_chip_final=1;
		elseif pchip_pin_tries = 2  then
			--pupdate_tok = pupdate_tok ||  ', pin_try_rem_chp=2 ';
			ppin_rem_chip_final=2;
		elseif pchip_pin_tries = 3 then
			--pupdate_tok = pupdate_tok ||  ', pin_try_rem_chp=3 ';
			ppin_rem_chip_final=3;
		end	if;	 
		
		if p3_1 = '91'   then   -- PIN UNBLOCK
			if  papprove_so_far = false then
					   	pvalid_notes = pvalid_notes   ||  ': pin unblock fail because other validations decline. ';
			else
				IF pinstr ='00' then pinstr =''; end if;
					if p4 = 0  then	
						p39o = '00'; --85
						prsn_cde = '265';
						else
						p39o = '00';
					end if; 
				raise notice 'Pin unblock requested From ATM. Card Flags before change %',ptok_flags;
				
				pvalid_notes = pvalid_notes ||  ': Pin unblock. ' ;
				
					pinstr =  pinstr  ||   '02;'	    ||  encode(ppvv,'hex')  || ';' ; --  +  SUBSTRing(CAST(@pvv AS VARCHAR(36)),3,36) + ';'
					--pupdate_tok = pupdate_tok   ||   ', tok_flags = tok_flags & B''10101111111111111111111111111111'', status_nwk = 1000  ';  -- means  update p_tok set tok_flags = tok_flags & B'10111111111111111111111111111111' where pr_tok = p2to ;
					--ROH 2020-11-09 removing dynamic update on p_tok 
					ptok_flags_final=ptok_flags_final & B'10101111111111111111111111111111';
					--pstatus_nwk_final=1000;
				
				INSERT INTO p_pvv_history   (pr_tok ,pvv,key_id ,cre_dt, note)
				VALUES  (p2to, ppvv, pkey_id, now(), 'Pin unblock requested From ATM. Card Flags before change');
			 END if ; ---end of if approve part 
		-- soh 09_dec_2019  End if moved from here to below ***
		
		else
--		pvalid_notes = pvalid_notes ||  ': soh 78 ' ;
			-- pin unblock via internal request  
			-- SOH 09_Dec_2019 add pAC clause to endsure pin unblock not sent in non-emv cases
			raise notice 'ptok_flags %',ptok_flags;
			if substring(ptok_flags,4,1) =1::bit and  pAC =1 then
				IF pinstr ='00' then pinstr =''; end if;
				pinstr = pinstr  ||    '02;'   ||  encode(ppvv,'hex') || ';' ;    --	  +  SUBSTRing(CAST(@pvv AS VARCHAR(36)),3,36) + ';'
				pvalid_notes = pvalid_notes ||  ': Pin unblock via internal Request ' ;
				--set @update_tok = @update_tok   +   ', tok_flags = tok_flags & B''01111111111111111111111111111111''';  -- means update p_tok set tok_flags = tok_flags & B'01111111111111111111111111111111' where   pr_tok = p2to  ;
				
				raise notice 'tok flag already assigned %',pupdate_tok;
				--pupdate_tok = pupdate_tok   ||   ', tok_flags = tok_flags & B''11101111111111111111111111111111'', status_nwk = 1000   ';  
				--ROH 2020-11-09 removing of dynamic update on p_tok
				ptok_flags_final=ptok_flags_final & B'11101111111111111111111111111111';
				--pstatus_nwk_final=1000;

				INSERT INTO p_pvv_history   (pr_tok ,pvv,key_id ,cre_dt, note)
				VALUES  (p2to, ppvv, pkey_id, now(), 'Pin unblock requested internally. Card Flags before change');

			end if;		
		end if ; ---end of top pin unblock check
      end if;  --soh 09_Dec_2019 end if moved to here from ***
--end if;
---------pin change-------------------
---------------------- END NEW PIN Management --------------------

-----------offline pin check 
if pAC<>9 and papprove_so_far is true then 
	--poffline_pin_check  ROH:2021-06-25
	--- SOH feb 19 revisions to pin tries declines ---
	if poffline_pin_check=1 then ---check offline pin -- is NULL then
	     --pchip_pin_tries
		if (ppin_rem_chp<1 or ppin_rem_onl <1 )  and papprove_so_far = true  then -- and (@3_1 <> '91' or @tok_flags & 268435456 =268435456)		then     
				 
				 if octet_length(p52)>1 then 
				 --pin avaible 
				 
				 else 
					 --pin not avaiable 
					  
					 p39o = '05';
					 p38o = Null;
					 prsn_cde = '225';
					 if ppin_rem_chp<1 or pchip_pin_tries<1 then
					 --offline pin counter 
						pvalid_notes = pvalid_notes || ': Chip pin tries exceeded via previous attempts';
					 end if;	
					 papprove_so_far = 0;
				 end if; --pin length check 
				 
				 
				 
				 if ppin_rem_onl<1 then
					 pvalid_notes = pvalid_notes || ': Online pin tries exceeded via previous attempts';
				 END IF;
				 
		END if;
	END if;
END if;	

			 raise notice 'test here';

 if pAC=0 then
	-- by Anil
	txn_checks_results := txn_checks_results | 281474976710656::bigint; -- BIT 49 ac_crypto_fail
 
	if  p55_9f27 = '\x80' then
		prsn_cde = '199';
	--6. Crypto fail... if @AC=0 then do: @48_74 = '50I', @39o= 57 (why not 88), @note = 'Cryptographic Failure => Decline' 
		
		p39o = '57'  ;
		papprove_so_far=0;
		p38o = NULL;
		p48o = p48o || '740350I';
		pvalid_notes = pvalid_notes  || ': ARQC match fail => @39o=57 ';
	
	else
   -- Not ARQC Cryptogram
	if p55_9f27 is NOt NULL then
		p39o = '57';
		papprove_so_far=0;
		p38o = NULL;
		prsn_cde = '196';
		p48o = p48o || '740350G';
		pvalid_notes = pvalid_notes  || ':  Crypto type incorrect ';
	END if;
 END if;
END if;
-------------------- HARDCODED COUNTRY BLOCKER ----------------- SOH 31 Aug 2018 ---------------

pcountry= Substring (p43, 38, 3);
-- changes by Anil 09 sept 2019 (txn_checks)
txn_checks_applied := txn_checks_applied | 562949953421312::bigint; -- BIT 50 banned_country check

if pcountry in ('SDN') then
	p39o = '05';
	papprove_so_far=0;
	p38o = NULL;
	prsn_cde = '291';
	pvalid_notes = pvalid_notes  || ':  Ban on this Country ';
	txn_checks_results := txn_checks_results | 562949953421312::bigint; -- BIT 50 banned_country_detected
END if;

----------------------------------------------------------------------------------

----------------------------Update on p_tok table -----------------------------------
 --set @update_tok = 1  

 -- SOH modified 2 feb 19  - improved pin count handling
 --ROH 2020-11-09 removing of dynamic update on p_tok 
 --raise notice 'checking pin ';
--if pupdate_tok <> '' then
   
-- 	if (length(p52) > 0 And papprove_so_far = true ) then 
	
--         if strpos('pin_try_rem_onl',pupdate_tok) > 0 then
-- 			pupdate_tok = REPLACE(pupdate_tok,', pin_try_rem_onl = pin_try_rem_onl-1 ', ', pin_try_rem_onl=3 ');
-- 		else
-- 		--else if @52 >0   ------- NEED TO ADD DE55 offline pin verified ok check in here
-- 		 pupdate_tok = pupdate_tok ||  ', pin_try_rem_onl=3 ';
		 
-- 		 end if;
-- 	END	if;	
 
-- 		if LEFT(pupdate_tok,1) = ','  then
-- 		  pupdate_tok = SUBSTRING (pupdate_tok,2,500)  ; 
--           pupdate_tok = 'update p_tok set ' || pupdate_tok || ' where pr_tok = ' || p2to::varchar;
--         --set @valid_notes = @valid_notes + @update_tok
		
-- 		EXECUTE  pupdate_tok  ;
--         --EXECUTE format('SELECT the_integer_field FROM %I WHERE %I NOT IN (%L,  %L)', tname,   cname,    'AK', 'CK');
-- 		end if;
-- end if;

--ROH --2020-11-09 --copied this from above Shane chagne 
if (length(p52) > 0 AND papprove_so_far = true ) then 
raise notice 'setting pin counter to 3 ';
ppin_rem_onl_final =3;
end if;

--ROH 2020-11-09
----updating into p_tok table 

update p_tok set 
   atc= pact_final,
   pin_try_rem_onl=ppin_rem_onl_final,
   pin_try_rem_chp=ppin_rem_chip_final,
   status_nwk= pstatus_nwk_final,
   de38=p38_final,
   tok_flags=ptok_flags_final,
   pvv=ppvv_final,
   npvv=ppvv_hsm_final

where pr_tok= p2to;  

----------------------------------------------------------------------------------------------

 -- account 
 --ntw_status

 ----------------------------------------------------------------------------------------------------------   
-- if update_tok <> '' then
--         update_tok := trim(leading ',' from update_tok);
--         pvalid_notes := pvalid_notes ||'update p_tok set ' || update_tok || ' where pr_tok = ' || p2to ;
--         raise notice '% %',update_tok,p2to;
-- 		EXECUTE 'update p_tok set ' || update_tok || ' where pr_tok = ' || p2to ;
-- 		--pin_try_rem_onl = pin_try_rem_onl-1 
--         --EXECUTE format('SELECT the_integer_field FROM %I WHERE %I NOT IN (%L,  %L)', tname,   cname,    'AK', 'CK');
-- end if;
---------------------  store to db ----------------------------------------------------------
--p39o ='62';asad
if pmtid ilike '%120' or  pmtid ilike '%420' then
--prsn_cde := p60_1;
p39o := '00';
p38o := Null;
p48o = Null;
end if;
--pacc_ext_ref =pacc_id;

-----------------Multi-Account / Wallets Logic ---------------------------
if penable_multi_acc_card='y' and pprod_id<>486 then
	--select * from p_acc_tok limit 10
	drop table IF EXISTS temp_p_acc_tok CASCADE;
	CREATE TEMP TABLE temp_p_acc_tok(
	pr_tok bigint,ac_id bigint,currency char(3),is_default smallint,active_currency smallint
	);
	insert into temp_p_acc_tok(pr_tok,ac_id,currency,is_default,active_currency )
	select pr_tok,ac_id,currency,is_default,active_currency from p_acc_tok where pr_tok=p2to;
   -- -- -- If (Transaction Currency DE49)  = (Supported wallet currency) 
   -- -- -- Then{
   -- -- -- Deduct Transaction amount(DE4) from corresponding wallet
   -- -- -- }
   -- -- -- Else if Billing amount(DE51)=IS_DEFAULT  {
   -- -- -- Deduct Billing amount(DE6) from corresponding currency wallet
   -- -- -- }
   ---------else { 
   ----------reject the transaction
   -----------  }
	select ac_id into pmulti_account from temp_p_acc_tok where currency=p49;-- and is_default=1 and active_currency=1;
	if pmulti_account is null then 
		--supported wallet not found 
		raise notice 'No supported wallet found';
		select ac_id into pmulti_account from temp_p_acc_tok where currency=p51;

	end if;
	--end if;   
	--   select ac_id into pacc_id from temp_p_acc_tok where currency=p49;-- and is_default=1 and active_currency=1;

	-- p51

	-- DROP TABLE temp_p_acc_tok;

	--------------------------------------------------------------------------
	raise notice 'Multi account id % and account id %',pmulti_account,pacc_id;
	if pmulti_account is not null then 
		pacc_id=pmulti_account;
	else 
		p39o='05';
		pvalid_notes = pvalid_notes  || ' valid multi account not found';

	end if; 

end if;---top check of multi-currency part 

--if pclient_id in (439480, 748842) then   -- added 748842 to support the boubyan client
 select ac_ext_ref into pacc_ext_ref from p_accounts where ac_id = pacc_id; --389-74961581811

---for manigo client , if ac_ext_ref is null then provide the ac_id in the pacc_ext_ref
--https://support.banking.live/jira/browse/DEV_POWER-408
if (pacc_ext_ref is null or pacc_ext_ref='' ) and pclient_id = 239741 then --replace with manigo client id 
pacc_ext_ref =pacc_id;
end if;

-- else 
-- pacc_ext_ref =pacc_id;
-- end if;
--p39o ='85';

---------ROh temp changes for MDES yellow path on------------------
-- if p2to=698492903 and p4=0 and p39o in ('00') then 
-- p39o ='85';
-- end if;

if substring(p124,1,2)='TA' and p39o in ('10','00','87','85')   and p4=0 then 
p39o='85';
pvalid_notes = pvalid_notes  || ':  MDES Yellow path  ';

ELSIF substring(p124,1,2) in ('AC','TC','TV')   then ---ACN, TCN, TVN
---Approve ACN,TCN,TVN message no matter what
p39o='00';
pvalid_notes = pvalid_notes  || ':  MDES  '  || substring(p124,1,2) || 'Transaction';

end if ;  

-----HOT FIX turning OFF RULES for JETCO Transaction-------------
---------2020-05-21 --- ROH ------------------------------------
if 	pcn='JTCO' then
	raise notice 'JETCO transaction';
	prls_applied=0;
	
    --Addition by Abhishek Bhujel to add issuer generated script for Jetco in MNQ
    if substring(ptok_flags,7,1)= 1::bit and pmtid = '0200' then
		select script, status,unupdated_counter into script_data,script_status,p_unupdated_counter from p_tok_script_queue where pbtok = ppb_tok;
		if script_data <> '' and papprove_so_far then
            if pinstr='00' then
                pinstr='';
            end if;

            currentStatus := mod(script_status,10);
            if currentStatus = 0 then
                script_status := script_status + div(script_status,10);
                p_unupdated_counter := 0;
            else
            	script_status := div(script_status,10)*10 + currentStatus;
                p_unupdated_counter := p_unupdated_counter+1;
                
            end if;

            pinstr:= pinstr||'03;'||script_data||';';
            update p_tok_script_queue set status = script_status, unupdated_counter = p_unupdated_counter where pbtok = ppb_tok;
            raise notice 'Updating user defined script during transaction';
		end if;   
	end if;
end if;

--Addition by Abhishek for Fast 2.0
if (p18 = '') is not true then
	select p18 || ' - '||mcc_desc into o_merchantType from m_mcc where mcc = p18::int;
end if;
if(p49 = '') is not true then
	select ccy_cde into o_transactionCurrency from m_currency where cde = p49;
end if;

if (p51 = '') is not true then
	select ccy_cde into o_billingCurrency from m_currency where cde = p51;
end if;

---ROH Temp setting for turning off rules for volume test
--     if p2to=927461846 then 
-- 	prls_applied='0';
-- 	end if;
if p2to=946946301 then 
	pauth_type=0;
	pclient_id=123456;
end if;

raise notice ' latest changes for Fast 2.0 % - % - %', o_merchantType, o_transactionCurrency, o_billingCurrency;

---------ROh temp changes for rules_flag on------------------
INSERT INTO Txn_Tok(tt_id,pr_uid,Txn_id,session_id,Network,AC,CV,CT,MTID,DE2
           ,DE3_S1,DE3_S2 ,DE3_S3,DE7
      ,DE9,DE10,DE11,DE12,DE13,DE14,DE15,DE16,DE18,DE20
      ,DE22_S1,DE22_S2,DE23,DE25,DE26,DE32,DE33
      ,DE35,DE37,DE38,DE39,DE41,DE42,DE43,DE44,DE45,DE48
      ,DE48o,DE49,DE50,DE51,DE53_S1,DE53_S2,DE53_S3,DE53_S4
      ,DE60_S1,DE60_S2,DE60_S3
      ,DE61_S1,DE61_S2,DE61_S3,DE61_S4,DE61_S5,DE61_S6,DE61_S7,DE61_S8
      ,DE61_S9,DE61_S10,DE61_S11,DE61_S12,DE61_S13,DE61_S14,DE62,DE63_S1
      ,DE63_S2,DE102,DE103,DE112,DE120_pc,DE120_addr,DE124,DE125, validation_notes,
      "9F36_ATC", "95_TVR", "9F34_CVM", "9F33_TCap","9F10_IAD",rsn_cde,txn_checks_applied,txn_checks_results,cre_dt)
       VALUES(pprid ,ppr_uuid,CAST(ptxn_id AS BIGINT),ppwr_instance_id,pcn ,pAC,pCV,pCT,pMTID,p2to,p3_1,p3_2 ,p3_3,to_timestamp(p7o, 'YYYYMMDDHH24MISS')
      ,p9,CAST(p10 AS DOUBLE PRECISION),p11,p12,p13,p14,p15,p16,CAST(p18 AS SMALLINT),p20,p22_1,p22_2,p23,p25,p26,p32,p33
      ,p35,p37,p38o,p39o,p41,p42,p43,p44,p45,p48_san,p48o,p49,p50,p51,p53_1,p53_2,p53_3,p53_4
      ,p60_1,p60_2,p60_3,p61_1,p61_2,p61_3,p61_4,p61_5,p61_6,p61_7,p61_8
      ,p61_9,p61_10,p61_11,p61_12,p61_13,p61_14,p62,p63_1
      ,p63_2,p102,p103,p112,p120_pc,p120_addr,p124,p125, pvalid_notes,get_byte(p55_9f36,1), p55_95, p55_9f34, p55_9f33,p55_9F10,prsn_cde,txn_checks_applied,txn_checks_results,pmsg_arrival_time);
            
      -- prid := currval('Txn_Tok_seq');  
	  prid := pprid;

			------------returning output -------------------------------------
			RETURN query
			select 
			p38o ,
			p39o ,
			p2to ,
			p48o ,
			ptxn_id, 
			pexp ,
			pinstr, 
			p44o ,
			pp_info, 
			pauth_type, 
			pclient_id,
			prid ,
			p7o ,
			p12_13o, 
			prsn_cde ,
			prls_applied ,
			ppb_tok ,
			p112o ,
			pprod_id ,
			pvalid_notes, 
			pstat_nwk ,
			pacc_ext_ref,
            txn_checks_applied,
			txn_checks_results,
			rule_level,
            pacc_id,
			o_merchantType,
            o_transactionCurrency,
            o_billingCurrency;
			--------------------------------------
       EXCEPTION
       WHEN OTHERS
       THEN
		RAISE NOTICE '********inserting faailed in Txn_Tok table  ***********';   
		raise notice '% %', SQLERRM, SQLSTATE;
		GET STACKED DIAGNOSTICS pvalid_notes :=  MESSAGE_TEXT  ;
		insert into user_logs(user_name,log_time,calling_proc,calling_parms,note,
		success)values('pretok',localtimestamp,'pre_tok',pvalid_notes,SQLERRM,1);

		raise notice 'Insertion not done,due to exception  returning default result '	 ;
	 
	  ------ OVERRIDES ---
  -- p39o := '14';
--  p7o := '9999';
--p4 := NULL;
   --  p38o := '319941';
       -- p44o := '123432';
--p38o := Null;
    --   p48o :=    '130212345678987654321' ; --'3321E54240200002199932710';  
    --   papprove_so_far := true;
    -- pvalid_notes := coalesce(pvalid_notes,'')  || ': Override for test. ';
--p48o :=null;

------------returning output whem exception -------------------------------------
p39o ='05';
p38o =null;
ptxn_id=0;
RETURN query
select 
p38o ,
p39o ,
p2to ,
p48o ,
ptxn_id, 
pexp ,
pinstr, 
p44o ,
pp_info, 
pauth_type, 
pclient_id ,
prid ,
p7o ,
p12_13o, 
prsn_cde ,
prls_applied ,
ppb_tok ,
p112o ,
pprod_id ,
pvalid_notes, 
pstat_nwk ,
pacc_ext_ref,
txn_checks_applied,
txn_checks_results,
rule_level,
pacc_id,
o_merchantType,
o_transactionCurrency,
o_billingCurrency;
--------------------------------------
            
END;
$BODY$;

ALTER FUNCTION public.ppwr_tok_pre16(character, bytea, bytea, bytea, character, bigint, character varying, bigint, bigint, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character, character varying, character, character, character, character varying, character varying, character varying, character varying, character varying, character varying, character, character, character, bytea, character varying, character varying, bytea, bytea, bytea, bytea, bytea, bytea, character varying, character varying, character varying, character varying, bytea, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, bytea, character varying, character varying, bytea, integer, integer, integer, character, integer, bytea, character, bigint, integer, integer, timestamp without time zone, bigint)
    OWNER TO bluser;
